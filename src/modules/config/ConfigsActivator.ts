import { Facade, IModuleActivator } from "../../frameworck";
import { ConfigModel } from "./models/ConfigModel";

export class ConfigsActivator implements IModuleActivator {
    public activate(data?: any): void {
        Facade.global.registerSingleton(ConfigModel);
    }
}
