import { Loader, LoaderResource } from "pixi.js";
import { LoaderModel } from "..";
import {
  Application,
  Dictionary,
  Facade,
  IAssets,
  LoaderEventsApi,
  LoadingGroup,
  LoadIntentApi,
} from "../../../frameworck";

export class LoaderController {
  protected loader: Loader;
  protected model: LoaderModel;

  constructor() {
    this.loader = Application.LOADER();
    this.model = Facade.global.getSingleton(LoaderModel);
    LoadIntentApi.ON_LOAD(this.load, this);
  }

  public async load(assets: IAssets): Promise<void> {
    await this.loadGroup(assets.preload, 30);
    LoaderEventsApi.EMIT_GROUP_LOADING_COMPLETE(LoadingGroup.preload);
    await this.loadGroup(assets.load, 68);
    LoaderEventsApi.EMIT_GROUP_LOADING_COMPLETE(LoadingGroup.load);
    await this.loadTexturesToGPU();

    LoaderEventsApi.EMIT_LOADING_COMPLETE();
  }

  protected async loadTexturesToGPU() {}

  protected loadGroup(
    groupAssets: Dictionary<string>,
    maxProgressValue: number
  ): Promise<void> {
    if (!groupAssets) {
      this.emmitProgress(maxProgressValue);
      return Promise.resolve();
    }
    Object.values(groupAssets).forEach((value) => this.loader.add(value));

    let groupProgress = 0;
    const cbId = this.loader.onProgress.add(
      (loader: Loader, resource: LoaderResource) => {
        groupProgress =
          (maxProgressValue * loader.progress) / 100 - groupProgress;
        this.emmitProgress(this.model.progress + groupProgress);
        console.log("loading: " + resource.url);
        console.log("loading: " + resource.name);
        console.log("total progress: " + this.model.progress + "%");
      }
    );

    return new Promise<void>((resolve) => {
      this.loader.load(() => {
        this.loader.onProgress.detach(cbId);
        this.emmitProgress(maxProgressValue);
        resolve();
      });
    });
  }

  protected emmitProgress(value: number): void {
    this.model.progress = value;
    LoaderEventsApi.EMIT_LOADING_PROGRESS(value);
  }
}
