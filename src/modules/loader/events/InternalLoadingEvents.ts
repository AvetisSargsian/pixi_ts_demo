export const enum InternalLoadingEvents {
  PROGRESS = "InternalLoadingEvents.progress",
  GROUP_COMPLETE = "InternalLoadingEvents.groupComplete",
}
