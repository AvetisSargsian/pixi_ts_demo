import {
  InternalLoadingEvents,
  LoaderController,
  LoaderModel,
  LoaderScene,
} from "..";
import { Assets, BaseView, Facade, SceneMediator } from "../../../frameworck";

export class LoaderMediator extends SceneMediator {
  protected model: LoaderModel;
  protected loaderController: LoaderController;

  public constructor(view?: BaseView) {
    super(view);

    this.model = Facade.global.getSingleton(LoaderModel);
    this.model.on(InternalLoadingEvents.PROGRESS, this.onLoadProgress, this);
    this.loaderController = Facade.global.getSingleton(LoaderController);
  }

  public dispose() {
    this.model.off(InternalLoadingEvents.PROGRESS, this.onLoadProgress, this);
    super.dispose();
  }

  protected onAddedToStage(): void {
    super.onAddedToStage();
    this.loaderController.load(Assets);
  }

  protected setNativeVIew(): BaseView {
    return Facade.global.getClassInstance(LoaderScene) as LoaderScene;
  }

  private onLoadProgress(value: number) {
    (this.nativeVIew as LoaderScene).updateText(value.toString());
  }
}
