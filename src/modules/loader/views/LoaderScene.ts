import { Text } from "pixi.js";
import { Application, BaseView } from "../../../frameworck";

export class LoaderScene extends BaseView {
  protected ptext: Text;

  constructor() {
    super();
    this.ptext = new Text("Loading 0 %");
  }

  public updateText(value: string) {
    // альтернатива для сцены прилоадера
    // let sprite = new PIXI.Sprite(PIXI.Texture.from("assets/snake.png"));
    // this.app.stage.addChild(sprite);

    this.ptext.style.fill = 0x00ff00;
    this.ptext.style.fontSize = 36;
    this.ptext.anchor.set(0.5, 0.5);
    this.ptext.text = "Loading " + value + " %";
  }

  protected onAdded() {
    super.onAdded();
    this.ptext.x = Application.GAME_WIDTH / 2;
    this.ptext.y = Application.GAME_HEIGHT / 2;
    this.addChild(this.ptext);
  }
}
