import { InternalLoadingEvents } from "..";
import { Model } from "../../../frameworck";

export class LoaderModel extends Model {
  private loadingProgress: number = 0;

  public get progress(): number {
    return this.loadingProgress;
  }

  public set progress(value: number) {
    this.loadingProgress = value;
    this.emit(InternalLoadingEvents.PROGRESS, this.loadingProgress);
  }
}
