import { LoaderController, LoaderMediator, LoaderModel, LoaderScene } from ".";
import { Facade, IModuleActivator } from "../../frameworck";

export class LoaderActivator implements IModuleActivator {
  public activate(data?: any): void {
    Facade.global.registerSingleton(LoaderModel);
    Facade.global.registerSingleton(LoaderController);

    Facade.global.registerClassAlias(LoaderScene);
    Facade.global.registerClassAlias(LoaderMediator);
  }
}
