package mvc.messanger {
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import mvc.command.AsyncCommand;
	
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.text.TextFormat;
	import starling.textures.Texture;
	import starling.utils.Align;
	import starling.utils.Color;

	public class MessageCommand extends AsyncCommand {
		private var _canvas:DisplayObjectContainer;
		private var _viewsContainer:DisplayObjectContainer
		private var _timer:Timer;
		private var _textField:TextField;
		private var _bgImage:Image; 
		
		public function MessageCommand(message:String, canvas:DisplayObjectContainer, bgTexture:Texture, duration:int, textColor:uint = Color.WHITE) {
			super();
			
			_canvas = canvas;
			_viewsContainer = new Sprite();
			_viewsContainer.touchable = false;
			_bgImage = new Image(bgTexture);
			_bgImage.touchable = false;
			_bgImage.scale9Grid = new Rectangle(20,20,360,60);
			
			_timer = new Timer(duration * 1000, 1);
			_timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			
			_textField = new TextField(100,50,message);
			_textField.touchable = false;
			_textField.autoSize = starling.text.TextFieldAutoSize.HORIZONTAL;
			var textFormat:TextFormat = new TextFormat("Verdana",18);
			textFormat.horizontalAlign   = starling.utils.Align.CENTER;
			textFormat.color = textColor;
			_textField.format = textFormat;
			
			_bgImage.width = _textField.bounds.width + 50;
			_textField.x = (_bgImage.width>>1) - (_textField.bounds.width>>1);
			_textField.y =(_bgImage.height>>1) - (_textField.bounds.height>>1);
			_viewsContainer.addChild(_bgImage);
			_viewsContainer.addChild(_textField);
		}
		
		override public function execute(data:Object = null):* {
			_canvas.addChildAt(_viewsContainer,_canvas.numChildren);
			_viewsContainer.x = _canvas.width/2 - _viewsContainer.width/2;
			_viewsContainer.y = _canvas.height/2 - _viewsContainer.height/2;
			_timer.start();
		}
		
		override public function complete(data:Object=null):void {
			_timer.stop();
			_timer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			_canvas.removeChild(_viewsContainer, true);
			super.complete(data);
		}
		
		override public function dispose():void {
			_timer 			= null;
			_canvas 		= null;
			_textField 		= null;
			_viewsContainer = null;
			super.dispose();
		}
		
		private function onTimerComplete(event:TimerEvent):void {
			complete();
		}
	}
}