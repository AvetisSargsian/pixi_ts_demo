import { Facade, IModuleActivator } from "../../frameworck";
import { LayoutController } from "./controller/LayoutController";
import { LayoutModel } from "./model/LayoutModel";

export class LayoutActivator implements IModuleActivator {
  public activate(data?: any): void {
    Facade.global.registerSingleton(LayoutModel);
    Facade.global.registerSingleton(LayoutController);
  }
}
