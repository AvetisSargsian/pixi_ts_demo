import { BaseView, MainStage, Model } from "../../../frameworck";

export class LayoutModel extends Model {
  protected stage: MainStage;
  protected _bottomLayout: BaseView;
  protected _midleLayout: BaseView;
  protected _topLayout: BaseView;

  public init(stage: MainStage): void {
    this.stage = stage;
    this._bottomLayout = new BaseView();
    this.stage.addChild(this._bottomLayout);
    this._midleLayout = new BaseView();
    this.stage.addChild(this._midleLayout);
    this._topLayout = new BaseView();
    this.stage.addChild(this._topLayout);
  }

  public get bottomLayout(): BaseView {
    return this._bottomLayout;
  }

  public get midleLayout(): BaseView {
    return this._midleLayout;
  }

  public get topLayout(): BaseView {
    return this._topLayout;
  }
}
