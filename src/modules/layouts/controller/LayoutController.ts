import { LayoutModel } from "..";
import {
  Application,
  BaseView,
  Facade,
  LayoutEventApi,
  LayoutIntentsApi,
  LayoutList,
} from "../../../frameworck";

export class LayoutController {
  protected model: LayoutModel;

  constructor() {
    this.model = Facade.global.getSingleton(LayoutModel);
    this.model.init(Application.STAGE());
    LayoutIntentsApi.ON_ADD_VIEW(this.addView, this);
  }

  protected addView(layout: LayoutList, view: BaseView): void {
    switch (layout) {
      case LayoutList.BOTTOM: {
        this.addChild(this.model.bottomLayout, view);
        break;
      }
      case LayoutList.MIDLE: {
        this.addChild(this.model.midleLayout, view);
        break;
      }
      case LayoutList.TOP: {
        this.addChild(this.model.topLayout, view);
        break;
      }
    }
  }

  protected addChild(parrent: BaseView, child: BaseView): void {
    parrent.addChild(child);
    LayoutEventApi.EMIT_VIEW_ADDED(child);
  }
}
