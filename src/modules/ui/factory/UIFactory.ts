import { SCALE_MODES } from "pixi.js";
import {
  Button,
  ButtonProperties,
  Switch,
  UIController,
  UIElement,
  UIProperties,
  UITypes,
} from "..";
import { Application, Facade, Graphics, Texture } from "../../../frameworck";
import {
  SwitchProperties,
  TextureProperties,
} from "../interfaces/UIProperties";

export class UIFactory {
  protected controller: UIController = Facade.global.getSingleton(UIController);

  public createTexture(textureProp: TextureProperties): Texture {
    const bg = new Graphics();
    // bg.lineStyle(1, 0xFFFFFF, 1);
    bg.beginFill(textureProp.color, 1);
    bg.drawRect(0, 0, textureProp.width, textureProp.height);
    bg.endFill();
    return Application.RENDERER().generateTexture(bg, SCALE_MODES.LINEAR, 1);

    // const renderer = Application.RENDERER();
    // const renderTexture = PIXI.RenderTexture.create(800, 600);
    // const sprite = PIXI.Sprite.from("spinObj_01.png");
    // sprite.position.x = 800 / 2;
    // sprite.position.y = 600 / 2;
    // sprite.anchor.x = 0.5;
    // sprite.anchor.y = 0.5;
    // renderer.render(sprite, renderTexture);
  }

  public createUiButton(btnProp: ButtonProperties): Button {
    return this.produceElement<Button>(UITypes.BUTTON, btnProp);
  }

  public createUiSwitch(switchProp: SwitchProperties): Switch {
    return this.produceElement<Switch>(UITypes.SWITCH, switchProp);
  }

  protected produceElement<T extends UIElement>(
    type: UITypes,
    props: UIProperties
  ): T {
    let element: UIElement;
    switch (type) {
      case UITypes.BUTTON: {
        const btnProp: ButtonProperties = props as ButtonProperties;
        element = new Button(
          btnProp.upState,
          btnProp.text,
          btnProp.downState,
          btnProp.overState,
          btnProp.disabledState,
          btnProp.callBack
        );
        break;
      }
      case UITypes.SWITCH: {
        const switchProp: SwitchProperties = props as SwitchProperties;
        element = new Switch(
          switchProp.upState,
          switchProp.text,
          switchProp.downState,
          switchProp.overState,
          switchProp.disabledState,
          switchProp.callBack
        );
        break;
      }
      default:
        break;
    }
    element.setId(this.controller);
    return element as T;
  }
}
