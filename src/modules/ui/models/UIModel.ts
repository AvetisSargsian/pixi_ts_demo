import { UIElement } from "..";
import { UITypes } from "../views/UITypes";

export class UIModel {
  private elements: Map<UITypes, UIElement[]>;

  public constructor() {
    this.elements = new Map();
  }

  public addUiElement(element: UIElement): boolean {
    if (!this.elements.has(element.type)) {
      this.elements.set(element.type, []);
      this.elements.get(element.type).push(element);
      return true;
    } else if (this.elements.get(element.type).indexOf(element) < 0) {
      this.elements.get(element.type).push(element);
      return true;
    }
    return false;
  }

  public removeUiElement(element: UIElement): void {
    if (this.elements.has(element.type)) {
      const uiElements = this.elements.get(element.type);
      this.elements.set(
        element.type,
        uiElements.filter((el) => element.id !== el.id)
      );
    }
  }
}
