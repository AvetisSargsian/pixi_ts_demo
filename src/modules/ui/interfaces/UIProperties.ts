import { TextStyle, Texture } from "../../../frameworck";

export interface UIProperties {
  width?: number;
  height?: number;
  x?: number;
  y?: number;
}

export interface TextProperties extends UIProperties {
  text?: string;
  textStyle?: TextStyle;
}

export interface SwitchProperties extends ButtonProperties {}

export interface ButtonProperties extends UIProperties {
  upState: Texture;
  text?: string;
  downState?: Texture;
  overState?: Texture;
  disabledState?: Texture;
  textStyle?: TextStyle;
  callBack?: () => void;
}

export interface TextureProperties extends UIProperties {
  color?: number;
  width: number;
  height: number;
}
