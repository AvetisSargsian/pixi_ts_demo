import {Button} from "./Button";
import {ButtonState} from "./ButtonState";
import {UITypes} from "./UITypes";
import {InteractionEvent} from "pixi.js";

export class Switch extends Button {
  protected _type: UITypes = UITypes.SWITCH;

  protected onButtonDown(event: InteractionEvent) {
    if (this._state === ButtonState.DOWN) {
      this.state = ButtonState.UP;
    } else if (this._state === ButtonState.OVER) {
      this.state = ButtonState.DOWN;
    }
  }

  protected onButtonUp(event: InteractionEvent) {
    if (this.callBack) {
      this.callBack(this.id);
    }
  }

  protected onButtonOver(event: InteractionEvent) {
    if (this._state === ButtonState.DOWN) {
      return;
    }
    this.state = ButtonState.OVER;
  }

  protected onButtonOut(event: InteractionEvent) {
    if (this._state === ButtonState.OVER) {
      this.state = ButtonState.UP;
    }
  }
}
