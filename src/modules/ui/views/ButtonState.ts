export const enum ButtonState {
  /** The button's default state. */
  UP = "ButtonState.UP",
  /** The button is pressed. */
  DOWN = "ButtonState.DOWN",
  /** The mouse hovers over the button. */
  OVER = "ButtonState.OVER",
  /** The button was disabled altogether. */
  DISABLED = "ButtonState.DISABLED",
}
