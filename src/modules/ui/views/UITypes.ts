export const enum UITypes {
  NONE = "UITypes.NONE",
  BUTTON = "UITypes.BUTTON",
  SWITCH = "UITypes.SWITCH",
  TEXT_FIELD = "UITypes.TEXT_FIELD",
}
