import { InteractionEvent, Rectangle } from "pixi.js";
import {
  BaseView,
  Image,
  TextField,
  TextStyle,
  Texture,
} from "../../../frameworck";
import { ButtonState } from "./ButtonState";
import { UIElement } from "./UIElement";
import { UITypes } from "./UITypes";

export class Button extends UIElement {
  public get text(): string {
    return this._textField ? this._textField.text : "";
  }
  public set text(value: string) {
    if (value.length === 0) {
      if (this._textField) {
        this._textField.text = value;
        // this._textField.removeFromParent();
      }
    } else {
      this.createTextField(value);
      this._textField.text = value;

      if (this._textField.parent == null) {
        this._contents.addChild(this._textField);
      }
    }
  }

  public get state(): ButtonState {
    return this._state;
  }
  public set state(value: ButtonState) {
    if (this._state === value) return;
    this._state = value;
    this._contents.x = this._contents.y = 0;
    this._contents.scale.set(1);
    this._contents.alpha = 1.0;

    switch (this._state) {
      case ButtonState.DOWN:
        this.setStateTexture(this._downState);
        this._contents.alpha = this._alphaWhenDown;
        this._contents.scale.set(this._scaleWhenDown);
        this._contents.x =
          ((1.0 - this._scaleWhenDown) / 2.0) * this._body.width;
        this._contents.y =
          ((1.0 - this._scaleWhenDown) / 2.0) * this._body.height;
        break;
      case ButtonState.UP:
        this.setStateTexture(this._upState);
        break;
      case ButtonState.OVER:
        this.setStateTexture(this._overState);
        this._contents.scale.set(this._scaleWhenOver);
        this._contents.x =
          ((1.0 - this._scaleWhenOver) / 2.0) * this._body.width;
        this._contents.y =
          ((1.0 - this._scaleWhenOver) / 2.0) * this._body.height;
        break;
      case ButtonState.DISABLED:
        this.setStateTexture(this._disabledState);
        this._contents.alpha = this._alphaWhenDisabled;
        break;
      default:
        throw new Error("Invalid button state: " + this._state);
    }
  }

  public get overlay(): BaseView {
    if (this._overlay == null) {
      this._overlay = new BaseView();
    }
    this._contents.addChild(this._overlay); // make sure it's always on top
    return this._overlay;
  }

  public get scaleWhenDown(): number {
    return this._scaleWhenDown;
  }
  public set scaleWhenDown(value: number) {
    this._scaleWhenDown = value;
  }

  public get scaleWhenOver(): number {
    return this._scaleWhenOver;
  }
  public set scaleWhenOver(value: number) {
    this._scaleWhenOver = value;
  }

  public get alphaWhenDown(): number {
    return this._alphaWhenDown;
  }
  public set alphaWhenDown(value: number) {
    this._alphaWhenDown = value;
  }

  public get alphaWhenDisabled(): number {
    return this._alphaWhenDisabled;
  }
  public set alphaWhenDisabled(value: number) {
    this._alphaWhenDisabled = value;
  }

  public get enabled(): boolean {
    return this._enabled;
  }
  public set enabled(value: boolean) {
    if (this._enabled !== value) {
      this._enabled = value;
      this.state = value ? ButtonState.UP : ButtonState.DISABLED;
    }
  }

  public get textStyle(): TextStyle | Partial<TextStyle> {
    if (this._textField == null) {
      this.createTextField();
    }
    return this._textField.style;
  }

  public set textStyle(value: TextStyle | Partial<TextStyle>) {
    if (this._textField == null) {
      this.createTextField();
    }
    this._textField.style = value;
  }

  public get upState(): Texture {
    return this._upState;
  }
  public set upState(value: Texture) {
    if (value == null) {
      throw new Error("Texture 'upState' cannot be null");
    }

    if (this._upState !== value) {
      this._upState = value;
      if (
        this._state === ButtonState.UP ||
        (this._state === ButtonState.DISABLED && this._disabledState == null) ||
        (this._state === ButtonState.DOWN && this._downState == null) ||
        (this._state === ButtonState.OVER && this._overState == null)
      ) {
        this.setStateTexture(value);
      }
    }
  }

  public get downState(): Texture {
    return this._downState;
  }
  public set downState(value: Texture) {
    if (this._downState !== value) {
      this._downState = value;
      if (this._state === ButtonState.DOWN) {
        this.setStateTexture(value);
      }
    }
  }
  public get overState(): Texture {
    return this._overState;
  }
  public set overState(value: Texture) {
    if (this._overState !== value) {
      this._overState = value;
      if (this._state === ButtonState.OVER) {
        this.setStateTexture(value);
      }
    }
  }

  public get disabledState(): Texture {
    return this._disabledState;
  }
  public set disabledState(value: Texture) {
    if (this._disabledState !== value) {
      this._disabledState = value;
      if (this._state === ButtonState.DISABLED) {
        this.setStateTexture(value);
      }
    }
  }

  private static readonly MAX_DRAG_DIST: number = 50;
  protected _type: UITypes = UITypes.BUTTON;
  protected _state: ButtonState;
  private _upState: Texture;
  private _downState: Texture;
  private _overState: Texture;
  private _disabledState: Texture;
  private _contents: BaseView;
  private _body: Image;
  private _textField: TextField;
  private _textBounds: Rectangle;
  private _overlay: BaseView;
  private _scaleWhenDown: number;
  private _scaleWhenOver: number;
  private _alphaWhenDown: number;
  private _alphaWhenDisabled: number;
  private _useHandCursor: boolean;
  private _enabled: boolean;
  private _triggerBounds: Rectangle;

  public constructor(
    upState: Texture,
    text: string = "",
    downState: Texture = null,
    overState: Texture = null,
    disabledState: Texture = null,
    protected callBack?: (id?: number) => void
  ) {
    super();

    if (upState == null) {
      throw new Error("Texture 'upState' cannot be null");
    }

    this._upState = upState;
    this._downState = downState;
    this._overState = overState;
    this._disabledState = disabledState;

    this._state = ButtonState.UP;
    this._body = new Image(upState);
    // this._body.pixelSnapping = true;
    this._scaleWhenDown = downState ? 1.0 : 0.9;
    this._scaleWhenOver = this._alphaWhenDown = 1.0;
    this._alphaWhenDisabled = disabledState ? 1.0 : 0.5;
    this._enabled = true;
    this._useHandCursor = true;
    this._textBounds = new Rectangle(0, 0, this._body.width, this._body.height);
    this._triggerBounds = new Rectangle();

    this._contents = new BaseView();
    this._contents.addChild(this._body);
    this.addChild(this._contents);

    this.interactive = true;
    this.interactiveChildren = true;
    this.buttonMode = true;

    // Use mouse-only events
    // .on('mousedown', onButtonDown)
    // .on('mouseup', onButtonUp)
    // .on('mouseupoutside', onButtonUp)
    // .on('mouseover', onButtonOver)
    // .on('mouseout', onButtonOut)

    // Use touch-only events
    // .on('touchstart', onButtonDown)
    // .on('touchend', onButtonUp)
    // .on('touchendoutside', onButtonUp)

    // Mouse & touch events are normalized into
    // the pointer* events for handling different
    // button events.
    // PIXI.utils.
    this.on("pointerdown", this.onButtonDown, this);
    this.on("pointerup", this.onButtonUp, this);
    this.on("pointerupoutside", this.onButtonUp, this);
    this.on("pointerover", this.onButtonOver, this);
    this.on("pointerout", this.onButtonOut, this);

    this.text = text;
  }

  public dispose(): void {
    if (this._textField) {
      // this._textField.dispose();
    }
    // super.dispose();
  }
  public readjustSize(): void {
    // const prevWidth: number = this._body.width;
    // const prevHeight: number = this._body.height;
    // this._body.readjustSize();
    // const scaleX: number = this._body.width  / prevWidth;
    // const scaleY: number = this._body.height / prevHeight;
    // this._textBounds.x *= scaleX;
    // this._textBounds.y *= scaleY;
    // this._textBounds.width *= scaleX;
    // this._textBounds.height *= scaleY;
    // if (this._textField) { this.createTextField(); }
  }

  protected onButtonDown(event: InteractionEvent) {
    this.state = ButtonState.DOWN;
  }

  protected onButtonUp(event: InteractionEvent) {
    this.state = ButtonState.UP;
    // if (!touch.cancelled) { dispatchEventWith(Event.TRIGGERED, true); }
    if (this.callBack) {
      this.callBack(this.id);
    }
  }

  protected onButtonOver(event: InteractionEvent) {
    if (this._state === ButtonState.DOWN) {
      return;
    }
    this.state = ButtonState.OVER;
  }

  protected onButtonOut(event: InteractionEvent) {
    // if (this._state === ButtonState.DOWN ) {
    this.state = ButtonState.UP;
    // } else if (this._state === ButtonState.UP && isWithinBounds) {
    // this.state = ButtonState.DOWN;
    // }
  }

  private createTextField(text: string = ""): void {
    if (this._textField == null) {
      this._textField = new TextField(text);
      // this._textField.pixelSnapping = this._body.pixelSnapping;
      this._textField.interactive = false;
      this._textField.interactiveChildren = false;
      // this._textField.autoScale = true;
      // this._textField.batchable = true;
    }

    this._textField.width = this._textBounds.width;
    this._textField.height = this._textBounds.height;
    this._textField.x = this._textBounds.x;
    this._textField.y = this._textBounds.y;
  }

  private setStateTexture(texture: Texture): void {
    this._body.texture = texture ? texture : this._upState;
  }
}
