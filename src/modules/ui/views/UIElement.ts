import { UIController, UITypes } from "..";
import { BaseView } from "../../../frameworck";

export class UIElement extends BaseView {
  protected _type: UITypes = UITypes.NONE;
  protected _id: number = -1;

  public get type(): UITypes {
    return this._type;
  }

  public get id(): number {
    return this._id;
  }

  public setId(controller: UIController): number {
    if (this._id !== -1) {
      return this._id;
    }
    return (this._id = controller.registerElement(this));
  }
}
