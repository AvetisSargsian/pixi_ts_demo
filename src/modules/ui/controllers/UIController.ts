import { UIElement, UIModel } from "..";
import { Facade } from "../../../frameworck";

export class UIController {
  private idCounter: number = 0;
  private uiModel: UIModel;

  constructor() {
    this.uiModel = Facade.global.getSingleton(UIModel);
  }

  public registerElement(element: UIElement): number {
    return this.uiModel.addUiElement(element) ? this.generateUnicId() : -1;
  }

  protected generateUnicId(): number {
    return this.idCounter++;
  }
}
