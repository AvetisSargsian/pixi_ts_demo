import { UIController, UIFactory, UIModel } from ".";
import { Facade, IModuleActivator } from "../../frameworck";

export class UIActivator implements IModuleActivator {
  public activate(data?: any): void {
    Facade.global.registerSingleton(UIModel);
    Facade.global.registerSingleton(UIController);
    Facade.global.registerSingleton(UIFactory);
  }
}
