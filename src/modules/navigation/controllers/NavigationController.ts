import { NavigationModel } from "..";
import {
  BaseController,
  Facade,
  NavigationIntentsApi,
  SceneMediator,
} from "../../../frameworck";

export class NavigationController extends BaseController {
  protected model: NavigationModel;

  constructor() {
    super();
    this.model = Facade.global.getSingleton(NavigationModel);
    NavigationIntentsApi.ON_CHANGE_SCENE_TO(this.changeSceneTo, this);
  }

  public dispose(): void {
    NavigationIntentsApi.OFF_CHANGE_SCENE_TO(this.changeSceneTo, this);
    super.dispose();
  }

  protected changeSceneTo(sceneId: number | string) {
    this.model.currentSceneMediator = new SceneMediator();
  }
}
