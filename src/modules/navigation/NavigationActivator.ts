import { NavigationController, NavigationModel } from ".";
import { Facade, IModuleActivator } from "../../frameworck";

export class NavigationActivator implements IModuleActivator {
  public activate(data?: any): void {
    Facade.global.registerSingleton(NavigationModel);
    Facade.global.registerSingleton(NavigationController);
  }
}
