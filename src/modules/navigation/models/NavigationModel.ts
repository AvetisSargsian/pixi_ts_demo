import {
  LayoutIntentsApi,
  LayoutList,
  Model,
  NavigationEventApi,
  SceneMediator,
} from "../../../frameworck";

export class NavigationModel extends Model {
  protected _currentSceneMediator: SceneMediator;

  public get currentSceneMediator(): SceneMediator {
    return this._currentSceneMediator;
  }

  public set currentSceneMediator(value: SceneMediator) {
    if (this._currentSceneMediator) {
      this._currentSceneMediator
        .removeFromStage()
        .then(() => this.addCurrent(value));
    } else {
      this.addCurrent(value);
      NavigationEventApi.EMIT_SCENE_CHANGED(this._currentSceneMediator);
    }
  }

  protected addCurrent(value: SceneMediator) {
    this._currentSceneMediator = value;
    LayoutIntentsApi.EMIT_ADD_VIEW(
      LayoutList.BOTTOM,
      this._currentSceneMediator.getNativeVIew()
    );
  }
}
