package popupmanager.controller {
	import mvc.command.AsyncCommand;
	import mvc.command.interfaces.IAsyncCommand;
	import mvc.view.BaseView;
	
	import starling.display.DisplayObjectContainer;
	import starling.events.Event;

	public class PopupCommand extends AsyncCommand {
		
		private var _popupView:BaseView;
		private var _canvas:DisplayObjectContainer;
		
		public function PopupCommand(){}
		
		override public function init(params:Object = null):IAsyncCommand {
//			popupView:BaseView,canvas:DisplayObjectContainer
			this._popupView = params[0];//popupView;
			this._canvas = params[1];//canvas;
			_popupView.addEventListener(Event.REMOVED_FROM_STAGE,onRemoveFromStage);
			return this;
		}
		
		override public function dispose():void {
			if (_popupView){
				_popupView.removeEventListener(Event.REMOVED_FROM_STAGE,onRemoveFromStage);	
			}
			_popupView = null;
			_canvas = null;
			super.dispose();
		}
		
		override public function execute(data:Object = null):* {
			_canvas.addChildAt(_popupView,_canvas.numChildren);
		}
		
		private function onRemoveFromStage():void {
			complete();
		}
	}
}