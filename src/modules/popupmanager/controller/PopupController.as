package popupmanager.controller {
	import mvc.command.AsyncCommandExecuter;
	import mvc.command.interfaces.IAsyncCommandExecuter;
	import mvc.mediator.IMediator;
	import mvc.view.BaseView;
	
	import popupmanager.model.PopupModel;

	public class PopupController {
		private static var _instance:PopupController;
		private var queueManager:IAsyncCommandExecuter;
		
		public function PopupController(pvt:PrivateClass){}
		
		public static function showNow(viewMediator:IMediator):void {
			instance.queueManager.addFirst(_instance.createCommand(viewMediator.nativeVIew));
			_instance.queueManager.execute();
		}
		
		public static function putInLine(viewMediator:IMediator):void {	
			instance.queueManager.addLast(_instance.createCommand(viewMediator.nativeVIew));
			_instance.queueManager.execute();
		}
		
		public static function closeCurentPopup(force:Boolean = false):void {	
			PopupModel.instance.closeDialog();//данная команда сразу диспозит окно на сцене, не вызывая анимацию удаления 
		}
		
		public static function clearPopupsLine():void {	
			instance.queueManager.clearList();
			closeCurentPopup();
		}
		
		private static function get instance():PopupController {
			if (!_instance) {
				_instance = new PopupController(new PrivateClass());
				_instance.initialize();
			}
			return _instance;
		}
		
		private function createCommand(view:BaseView):PopupCommand {
			var command:PopupCommand = PopupModel.instance.getPopupCommand();
			command.init([view, PopupModel.instance.canvas]);
			return command;
		}
		
		private function initialize():void {
			queueManager = new AsyncCommandExecuter();
			(queueManager as AsyncCommandExecuter).garbageCollector = PopupModel.instance.returnUsedPopupCommand;
		}
	}
}
class PrivateClass{
	public function PrivateClass(){}
}