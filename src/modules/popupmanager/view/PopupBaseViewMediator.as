package popupmanager.view {
	import mvc.animator.Animator;
	import mvc.mediator.CompositeMediator;
	import mvc.view.BaseView;
	
	import popupmanager.model.PopupModel;
	
	import sounds.SoundManager;
	
	import starling.animation.Transitions;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.RenderTexture;
	import starling.utils.Color;
	
	public class PopupBaseViewMediator extends CompositeMediator {
		private static var RENDER_TEXTURE:RenderTexture;
		private var _bgBlocker:Image;
		protected var _isSubmit:Boolean;
		
		public function PopupBaseViewMediator(thisView:BaseView = null) {
			super(thisView);
			
			_nativeVIew.addEventListener(Event.TRIGGERED,onBtnPush);
		}
		
		override public function toString():String {
			return "PopupBaseViewMediator";
		}

		override public function dispose():void {
			PopupModel.instance.removeCallBack(PopupModel.CLOSE, onCloseComplete);//close
			_nativeVIew.removeEventListener(Event.TRIGGERED, onBtnPush);
			_bgBlocker.removeFromParent(true);
			_bgBlocker = null;
			SoundManager.clearSoundsLine();
			super.dispose();
		}
		
		override protected function setNativeVIew():BaseView {
			return new BaseView();
		}
		
		override protected function onAddedToStage(event:Event = null):void {
			super.onAddedToStage(event);
			
			PopupModel.instance.registerCallBack(PopupModel.CLOSE, onCloseComplete);//close
			
			if(RENDER_TEXTURE == null) {
				RENDER_TEXTURE = new RenderTexture(BaseFrameConstants.STAGE_WIDTH/8, BaseFrameConstants.STAGE_HEIGHT/8);
				var blocker:Quad = new Quad(BaseFrameConstants.STAGE_WIDTH/8, BaseFrameConstants.STAGE_HEIGHT/8, Color.BLACK);
				RENDER_TEXTURE.draw(blocker);
			}
		
			_bgBlocker = new Image(RENDER_TEXTURE);
			_bgBlocker.scale = 8.5;
			_bgBlocker.alpha = 0.1;
			contextView.addChildAt(_bgBlocker,0);
			show();
		}
		
		protected function onSubmit():void {
			_isSubmit = true;
			close();
		}
		
		protected function onCancel():void {
			_isSubmit = false;
			close();
		}
		
		protected function onShowComplete():void {}
		
		protected function onCloseComplete():void {		
			dispose();
		}
		
		protected function show():void {
			Animator.appear(_bgBlocker,0.4, 0.5);
			Animator.move(_nativeVIew,0.5,_nativeVIew.x,BaseFrameConstants.STAGE_HEIGHT/2,Transitions.EASE_OUT_BACK,onShowComplete);
			SoundManager.putInSoundsLine("window_appear_1");
		}
		
		protected function close(data:Object = null):void {
			Animator.appear(_bgBlocker,0.4,0);
			Animator.move(_nativeVIew,0.5,_nativeVIew.x,BaseFrameConstants.STAGE_HEIGHT,Transitions.EASE_IN_BACK, onCloseComplete);
			SoundManager.putInSoundsLine("window_disappear_1");	
		}
		
		private function onBtnPush(event:Event):void {
			var btn:Button = event.target as Button;
			if (btn) {
				switch(btn.name) {
					case PopupBaseView.CLOSE_BTN:
						close();
						event.stopImmediatePropagation();
						break;
					case PopupBaseView.CANCEL_BTN:
						onCancel();
						event.stopImmediatePropagation();
						break;
					case PopupBaseView.SUBMIT_BTN:
						onSubmit();
						event.stopImmediatePropagation();
						break;
				}
			}
		}
	}
}