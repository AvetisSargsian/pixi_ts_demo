package popupmanager.view {
	import globalmanagers.DisplayManager;
	
	import mvc.view.BaseView;
	
	public class PopupBaseView extends BaseView {
		
		public static const SCENE_BLOCKER:String = "SCENE_BLOCKER";
		public static const POPUP_CONTEINER:String = "POPUP_CONTEINER";
		public static const CLOSE_BTN:String = "CLOSE_BTN";
		public static const SUBMIT_BTN:String = "SUBMIT_BTN";
		public static const CANCEL_BTN:String = "CANCEL_BTN";
		
		public function PopupBaseView() {
			super();
			
			this.onAddedToStage = onAdded;
		}
		
		protected function onAdded():void {
			this.onAddedToStage = null;
			
			this.alignPivot();
			this.y = -this.height;
			this.x = DisplayManager.STARLING_STAGE_WIDTH/2;
		}
	}
}