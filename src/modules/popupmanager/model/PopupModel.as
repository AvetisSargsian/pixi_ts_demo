package popupmanager.model {
	import mvc.model.AbstractModel;
	import mvc.pool.DynamicMultiProductPool;
	import mvc.view.BaseView;
	
	import popupmanager.controller.PopupCommand;
	
	public class PopupModel extends AbstractModel {
		
		public static const CLOSE:String = "close";
		private static var _instance:PopupModel;
		
		private var _canvas:BaseView;
		private var curentDialog:BaseView;
		private var commandPool:DynamicMultiProductPool;
		
		public static function get instance():PopupModel {
			if (_instance == null){
				_instance = new PopupModel (new PrivateClass());		
			}
			return _instance;
		}
		
		public function PopupModel(pvt:PrivateClass) {
			super();
		}
		
		public function init(displayObject:BaseView):void {
			if (_canvas == null)
				_canvas = displayObject;
			commandPool = new DynamicMultiProductPool();
			commandPool.fillWithClass(PopupCommand, 10);
		}
		
		public function getPopupCommand():PopupCommand {
			return commandPool.pull(PopupCommand) as PopupCommand;
		}
		
		public function returnUsedPopupCommand(command:PopupCommand):void {
			commandPool.push(command);
		}
		
		public function get canvas():BaseView {
			return _canvas;
		}
		
		public function closeDialog():void {
			invokeCallBacks(CLOSE);
		}
	}
}
class PrivateClass {
	public function PrivateClass(){}
}