import {
  CapitalistGameController,
  CapitalistGameModel,
  CapitalistPersistenceManager,
  IPersistenceData,
} from "..";
import { BaseCommand, Facade } from "../../../frameworck";

export class SaveCapitalistProgressCommand extends BaseCommand {
  public execute() {
    const localFacade: Facade = Facade.getSpace("CapitalistGameClone");
    const gameModel: CapitalistGameModel =
      localFacade.getSingleton(CapitalistGameModel);
    const gameController: CapitalistGameController = localFacade.getSingleton(
      CapitalistGameController
    );
    const persistManager: CapitalistPersistenceManager =
      localFacade.getSingleton(CapitalistPersistenceManager);

    const data: IPersistenceData = {
      mainBalance: gameModel.balance,
      business: {} as any,
    };
    gameController.getRunningBusinesses().forEach((model) => {
      data.business[model.type] = model.getParameters();
    });

    persistManager.save(data);
  }
}
