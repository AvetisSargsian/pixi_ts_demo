import { BusinessModel } from "..";
import { BaseCommand } from "../../../frameworck";

export class LaunchBusinessCommand extends BaseCommand {
  public execute(model: BusinessModel): void {
    model.launch();
  }
}
