import { CapitalistGameModel, CapitalistPersistenceManager } from "..";
import { BaseCommand, Facade } from "../../../frameworck";

export class CapitalistRestoreGameCommand extends BaseCommand {
  public execute(data?: any) {
    const localFacade: Facade = Facade.getSpace("CapitalistGameClone");
    const persistManager = localFacade.getSingleton(
      CapitalistPersistenceManager
    );
    const gameModel: CapitalistGameModel =
      localFacade.getSingleton(CapitalistGameModel);

    gameModel.balance = persistManager.load().mainBalance;
  }
}
