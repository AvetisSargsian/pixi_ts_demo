import {
  BusinessCostPrices,
  BusinessModel,
  CapitalistGameController,
  CapitalistGameModel,
} from "..";
import { BaseCommand, Facade } from "../../../frameworck";

export class UpgradeBusinessCommand extends BaseCommand {
  public execute(model: BusinessModel) {
    const localFacade: Facade = Facade.getSpace("CapitalistGameClone");
    const gameModel: CapitalistGameModel =
      localFacade.getSingleton(CapitalistGameModel);
    const { upgradeLevel } = model.getParameters();
    if (gameModel.balance >= 200 * upgradeLevel) {
      gameModel.balance -= 200 * upgradeLevel;
      model.upgrade();
    }
  }
}
