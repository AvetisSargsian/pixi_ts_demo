import { BusinessModel, CapitalistGameController } from "..";
import { BaseCommand, Facade } from "../../../frameworck";

export class ActivateBusinessCommand extends BaseCommand {
  public execute(model: BusinessModel) {
    const localFacade: Facade = Facade.getSpace("CapitalistGameClone");
    localFacade.getSingleton(CapitalistGameController).addBusiness(model);
    model.tryAutoLaunch();
  }
}
