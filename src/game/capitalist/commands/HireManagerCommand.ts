import { BusinessManagersCosts, BusinessModel, CapitalistGameModel } from "..";
import { BaseCommand, Facade } from "../../../frameworck";

export class HireManagerCommand extends BaseCommand {
  public execute(model: BusinessModel) {
    if (model.haveManager) {
      return;
    }
    const localFacade: Facade = Facade.getSpace("CapitalistGameClone");
    const gameModel: CapitalistGameModel =
      localFacade.getSingleton(CapitalistGameModel);
    if (gameModel.balance >= BusinessManagersCosts.get(model.type)) {
      model.hireManager();
      model.launch();
      gameModel.balance -= BusinessManagersCosts.get(model.type);
    }
  }
}
