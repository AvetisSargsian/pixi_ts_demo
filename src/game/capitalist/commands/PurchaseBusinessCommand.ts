import {
  BusinessCostPrices,
  BusinessModel,
  CapitalistGameController,
  CapitalistGameModel,
} from "..";
import { BaseCommand, Facade } from "../../../frameworck";

export class PurchaseBusinessCommand extends BaseCommand {
  public execute(model: BusinessModel) {
    const localFacade: Facade = Facade.getSpace("CapitalistGameClone");
    const gameModel: CapitalistGameModel =
      localFacade.getSingleton(CapitalistGameModel);
    if (gameModel.balance >= BusinessCostPrices.get(model.type)) {
      localFacade.getSingleton(CapitalistGameController).addBusiness(model);
      model.enable();
      gameModel.balance -= BusinessCostPrices.get(model.type);
    }
  }
}
