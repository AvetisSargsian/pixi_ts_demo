export const enum CapitalistInternalEvents {
  GO_BACK_BUTTON_PRESSED = "CapitalistInternalEvents.GO_BACK_BUTTON_PRESSED",
  SAVE_PROGRESS = "CapitalistInternalEvents.SAVE_PROGRESS",
  RESTORE_PROGRESS = "CapitalistInternalEvents.RESTORE_PROGRESS",
}
