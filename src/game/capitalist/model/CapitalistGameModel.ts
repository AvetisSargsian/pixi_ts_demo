import { Model } from "../../../frameworck";

export class CapitalistGameModel extends Model {
  public static readonly BALANCE_UPDATE: string =
    "CapitalistGameModel.BALANCE_UPDATE";

  private _balance: number = 100;

  public get balance(): number {
    return this._balance;
  }

  public set balance(value: number) {
    this._balance = value;
    this.emit(CapitalistGameModel.BALANCE_UPDATE);
  }

  public init(): void {
    console.log("CapitalistGameModel");
  }
}
