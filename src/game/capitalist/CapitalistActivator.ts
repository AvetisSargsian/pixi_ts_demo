import { Facade, IModuleActivator } from "../../frameworck";
import { BusinessFactory } from "./business/factory/BusinessFactory";
import { BusinessViewMediator } from "./business/mediator/BusinessViewMediator";
import { PayoutProgressBarMediator } from "./business/mediator/PayoutProgressBarMediator";
import { BusinessModel } from "./business/model/BusinessModel";
import { BankBusinessView } from "./business/view/BankBusinessView";
import { BusinessView } from "./business/view/BusinessView";
import { CarWashBusinessView } from "./business/view/CarWashBusinessView";
import { DonatsShopBusinesView } from "./business/view/DonatsShopBusinesView";
import { HockeyTeamBusinessView } from "./business/view/HockeyTeamBusinessView";
import { LemonadeBusinessView } from "./business/view/LemonadeBusinessView";
import { MovieStudioBusinessView } from "./business/view/MovieStudioBusinessView";
import { NewspaperBusinessView } from "./business/view/NewspaperBusinessView";
import { OilCompanyBusinessView } from "./business/view/OilCompanyBusinessView";
import { PayoutProgressBarView } from "./business/view/PayoutProgressBarView";
import { PayoutTimerView } from "./business/view/PayoutTimerView";
import { PizzaDeliveryBusinessView } from "./business/view/PizzaDeliveryBusinessView";
import { ShrimpBoatBusinessView } from "./business/view/ShrimpBoatBusinessView";
import { ActivateBusinessCommand } from "./commands/ActivateBusinessCommand";
import { CapitalistRestoreGameCommand } from "./commands/CapitalistRestoreGameCommand";
import { HireManagerCommand } from "./commands/HireManagerCommand";
import { LaunchBusinessCommand } from "./commands/LaunchBusinessCommand";
import { PurchaseBusinessCommand } from "./commands/PurchaseBusinessCommand";
import { SaveCapitalistProgressCommand } from "./commands/SaveCapitalistProgressCommand";
import { UpgradeBusinessCommand } from "./commands/UpgradeBusinessCommand";
import { CapitalistGameController } from "./controller/CapitalistGameController";
import { CapitalistPersistenceManager } from "./controller/CapitalistPersistenceManager";
import { WindowEventsManager } from "./controller/WindowEventsManager";
import { BusinessEvents } from "./events/BusinessEvents";
import { CapitalistInternalEvents } from "./events/CapitalistInternalEvents";
import { CapitalistSceneMediator } from "./mediator/CapitalistSceneMediator";
import { CapitalistGameModel } from "./model/CapitalistGameModel";
import { CapitalistSceneView } from "./view/CapitalistSceneView";

export class CapitalistActivator implements IModuleActivator {
  public activate(data?: any): void {
    const localFacade: Facade = Facade.registerNameSpace("CapitalistGameClone");

    localFacade.registerClassAlias(BusinessModel, BusinessModel);
    localFacade.registerClassAlias(
      PayoutProgressBarView,
      PayoutProgressBarView
    );
    localFacade.registerClassAlias(PayoutTimerView, PayoutTimerView);
    localFacade.registerClassAlias(BusinessView, BusinessView);
    localFacade.registerClassAlias(LemonadeBusinessView, LemonadeBusinessView);
    localFacade.registerClassAlias(
      NewspaperBusinessView,
      NewspaperBusinessView
    );
    localFacade.registerClassAlias(CarWashBusinessView, CarWashBusinessView);
    localFacade.registerClassAlias(
      PizzaDeliveryBusinessView,
      PizzaDeliveryBusinessView
    );
    localFacade.registerClassAlias(
      DonatsShopBusinesView,
      DonatsShopBusinesView
    );
    localFacade.registerClassAlias(
      ShrimpBoatBusinessView,
      ShrimpBoatBusinessView
    );
    localFacade.registerClassAlias(
      HockeyTeamBusinessView,
      HockeyTeamBusinessView
    );
    localFacade.registerClassAlias(
      MovieStudioBusinessView,
      MovieStudioBusinessView
    );
    localFacade.registerClassAlias(BankBusinessView, BankBusinessView);
    localFacade.registerClassAlias(
      OilCompanyBusinessView,
      OilCompanyBusinessView
    );
    localFacade.registerClassAlias(
      PayoutProgressBarMediator,
      PayoutProgressBarMediator
    );
    localFacade.registerClassAlias(BusinessViewMediator, BusinessViewMediator);
    localFacade.registerSingleton(BusinessFactory);

    localFacade.registerSingleton(CapitalistPersistenceManager);
    localFacade.registerSingleton(CapitalistGameModel);
    localFacade.registerSingleton(CapitalistGameController);
    localFacade.registerSingleton(WindowEventsManager);
    localFacade.registerClassAlias(CapitalistSceneView, CapitalistSceneView);
    localFacade.registerClassAlias(
      CapitalistSceneMediator,
      CapitalistSceneMediator
    );

    localFacade.registerClassAlias(
      CapitalistRestoreGameCommand,
      CapitalistRestoreGameCommand
    );
    localFacade.registerClassAlias(
      ActivateBusinessCommand,
      ActivateBusinessCommand
    );
    localFacade.registerClassAlias(
      PurchaseBusinessCommand,
      PurchaseBusinessCommand
    );
    localFacade.registerClassAlias(
      LaunchBusinessCommand,
      LaunchBusinessCommand
    );
    localFacade.registerClassAlias(HireManagerCommand, HireManagerCommand);
    localFacade.registerClassAlias(
      UpgradeBusinessCommand,
      UpgradeBusinessCommand
    );
    localFacade.registerClassAlias(
      SaveCapitalistProgressCommand,
      SaveCapitalistProgressCommand
    );

    localFacade.bindCommandToEvent(
      BusinessEvents.BUSINESS_ACTIVATE,
      ActivateBusinessCommand
    );
    localFacade.bindCommandToEvent(
      BusinessEvents.BUSINESS_PURCHASE,
      PurchaseBusinessCommand
    );
    localFacade.bindCommandToEvent(
      BusinessEvents.BUSINESS_LAUNCH,
      LaunchBusinessCommand
    );
    localFacade.bindCommandToEvent(
      BusinessEvents.BUSINESS_HIRE_MANAGER,
      HireManagerCommand
    );
    localFacade.bindCommandToEvent(
      BusinessEvents.BUSINESS_UPGRADE,
      UpgradeBusinessCommand
    );
    localFacade.bindCommandToEvent(
      CapitalistInternalEvents.SAVE_PROGRESS,
      SaveCapitalistProgressCommand
    );
    localFacade.bindCommandToEvent(
      CapitalistInternalEvents.RESTORE_PROGRESS,
      CapitalistRestoreGameCommand
    );
  }
}
