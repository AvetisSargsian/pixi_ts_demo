import { Point } from "pixi.js";
import {
  BusinessTypes,
  CapitalistGameController,
  CapitalistGameModel,
  CapitalistInternalEvents,
  CapitalistSceneView,
} from "..";
import {
  BaseView,
  Facade,
  NavigationIntentsApi,
  SceneMediator,
  TextField,
} from "../../../frameworck";
import { BusinessFactory } from "../business/factory/BusinessFactory";

export class CapitalistSceneMediator extends SceneMediator {
  private localFacade: Facade = Facade.getSpace("CapitalistGameClone");
  private gameModel: CapitalistGameModel =
    this.localFacade.getSingleton(CapitalistGameModel);
  private businessFactory: BusinessFactory =
    this.localFacade.getSingleton(BusinessFactory);
  private depositTF: TextField;

  constructor() {
    super();
    this.localFacade.dispatcher.emit(CapitalistInternalEvents.RESTORE_PROGRESS);
  }

  public dispose(): void {
    this.removeInternalListeners();
    this.gameModel.dispose();
    super.dispose();
  }

  protected addInternalListeners(): void {
    this.nativeVIew.on(
      CapitalistInternalEvents.GO_BACK_BUTTON_PRESSED,
      this.onExitPressed,
      this
    );
    this.gameModel.on(
      CapitalistGameModel.BALANCE_UPDATE,
      this.onBalanceUpdate.bind(this)
    );
  }

  protected removeInternalListeners(): void {
    this.nativeVIew.off(
      CapitalistInternalEvents.GO_BACK_BUTTON_PRESSED,
      this.onExitPressed,
      this
    );
  }

  protected onAddedToStage(): void {
    this.addInternalListeners();

    this.depositTF = this.nativeVIew.getChildByName(
      CapitalistSceneView.DEPOSIT_TEXT
    ) as TextField;

    const lemon = this.businessFactory.produce(
      BusinessTypes.LEMONADE,
      this.nativeVIew,
      new Point(300, 60)
    );
    this.add(lemon);

    const newspaper = this.businessFactory.produce(
      BusinessTypes.NEWSPAPER,
      this.nativeVIew,
      new Point(300, 210)
    );
    this.add(newspaper);

    const carWash = this.businessFactory.produce(
      BusinessTypes.CAR_WASH,
      this.nativeVIew,
      new Point(300, 360)
    );
    this.add(carWash);

    const pizza = this.businessFactory.produce(
      BusinessTypes.PIZZA_DELIVERY,
      this.nativeVIew,
      new Point(300, 510)
    );
    this.add(pizza);

    const donats = this.businessFactory.produce(
      BusinessTypes.DONATS_SHOP,
      this.nativeVIew,
      new Point(300, 660)
    );
    this.add(donats);

    const shrimp = this.businessFactory.produce(
      BusinessTypes.SHRIMP_BOAT,
      this.nativeVIew,
      new Point(800, 60)
    );
    this.add(shrimp);

    const hokey = this.businessFactory.produce(
      BusinessTypes.HOCKEY_TEAM,
      this.nativeVIew,
      new Point(800, 210)
    );
    this.add(hokey);

    const movie = this.businessFactory.produce(
      BusinessTypes.MOVIE_STUDIO,
      this.nativeVIew,
      new Point(800, 360)
    );
    this.add(movie);

    const bank = this.businessFactory.produce(
      BusinessTypes.BANK,
      this.nativeVIew,
      new Point(800, 510)
    );
    this.add(bank);

    const oil = this.businessFactory.produce(
      BusinessTypes.OIL_COMPANY,
      this.nativeVIew,
      new Point(800, 660)
    );
    this.add(oil);

    this.localFacade.getSingleton(CapitalistGameController).execute();
    this.onBalanceUpdate();
  }

  protected setNativeVIew(): BaseView {
    return Facade.getSpace("CapitalistGameClone").getClassInstance(
      CapitalistSceneView
    ) as CapitalistSceneView;
  }

  private onExitPressed(btnId: number): void {
    this.localFacade.dispatcher.emit(CapitalistInternalEvents.SAVE_PROGRESS);
    NavigationIntentsApi.EMIT_CHANGE_SCENE_TO(btnId);
  }

  private onBalanceUpdate() {
    this.update(this.gameModel.balance);
    this.updateTF();
  }

  private updateTF() {
    this.depositTF.text = this.formatBalanceValue(this.gameModel.balance);
  }

  private formatBalanceValue(value: number): string {
    return value.toFixed(2);
  }
}
