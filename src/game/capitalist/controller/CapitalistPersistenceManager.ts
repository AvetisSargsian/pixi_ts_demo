import { BusinessInitValues } from "../business/constants/BusinessInitValues";
import { IPersistenceData } from "../interfaces/IPersistenceData";

export class CapitalistPersistenceManager {
  public readonly localStorageKey = "AdvCapitalistClone";

  constructor() {
    if (!localStorage.getItem(this.localStorageKey)) {
      const data: any = {
        mainBalance: 100,
        business: {},
      };
      this.save(data);
    }
  }

  public save(data: IPersistenceData): void {
    localStorage.setItem(this.localStorageKey, JSON.stringify(data));
  }

  public load(): IPersistenceData {
    return JSON.parse(localStorage.getItem(this.localStorageKey));
  }
}
