import { CapitalistInternalEvents } from "..";
import { Facade } from "../../../frameworck";

export class WindowEventsManager {
  constructor() {
    window.onbeforeunload = () => this.save();
    window.onblur = () => this.save();
  }

  protected save(): void {
    Facade.getSpace("CapitalistGameClone").dispatcher.emit(
      CapitalistInternalEvents.SAVE_PROGRESS
    );
  }
}
