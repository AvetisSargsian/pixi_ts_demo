import { BusinessModel, CapitalistGameModel } from "..";
import { BaseCommand, BaseController, Facade } from "../../../frameworck";

export class CapitalistGameController
  extends BaseController
  implements BaseCommand
{
  private businessStore: BusinessModel[] = [];
  private gameModel: CapitalistGameModel = Facade.getSpace(
    "CapitalistGameClone"
  ).getSingleton(CapitalistGameModel);

  public addBusiness(business: BusinessModel): void {
    this.businessStore.push(business);
  }

  public getRunningBusinesses(): BusinessModel[] {
    return this.businessStore;
  }

  public execute(data?: any) {
    this.startTick();
  }

  protected tick(dt: number) {
    const now: number = Date.now();
    this.businessStore.forEach((model: BusinessModel) => {
      if (model.isEnabled) {
        model.update(now);
        if (model.deposit > 0) {
          this.gameModel.balance += model.withdrewDeposit();
        }
      }
    });
  }
}
