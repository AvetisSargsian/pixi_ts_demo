import { Point } from "pixi.js";
import {
  BankBusinessView,
  BusinessInitValues,
  BusinessModel,
  BusinessTypes,
  BusinessView,
  BusinessViewMediator,
  CapitalistPersistenceManager,
  CarWashBusinessView,
  DonatsShopBusinesView,
  HockeyTeamBusinessView,
  IBusinessParameters,
  IPersistenceData,
  LemonadeBusinessView,
  MovieStudioBusinessView,
  NewspaperBusinessView,
  OilCompanyBusinessView,
  PizzaDeliveryBusinessView,
  ShrimpBoatBusinessView,
} from "../..";
import { BaseView, Facade } from "../../../../frameworck";

export class BusinessFactory {
  private localFacade: Facade = Facade.getSpace("CapitalistGameClone");

  public produce(
    type: BusinessTypes,
    container: BaseView,
    startPosition: Point
  ): BusinessViewMediator {
    const businessView: BusinessView = this.createView(type, startPosition);
    const businessModel: BusinessModel = this.createModel(type);
    const businessMediator: BusinessViewMediator = this.createMediator(
      businessView,
      businessModel
    );
    businessMediator.addToParent(container);
    return businessMediator;
  }

  protected createView(type: BusinessTypes, startPos: Point): BusinessView {
    let view: BusinessView;
    switch (type) {
      case BusinessTypes.LEMONADE:
        view = this.localFacade.getClassInstance(LemonadeBusinessView);
        break;
      case BusinessTypes.NEWSPAPER:
        view = this.localFacade.getClassInstance(NewspaperBusinessView);
        break;
      case BusinessTypes.CAR_WASH:
        view = this.localFacade.getClassInstance(CarWashBusinessView);
        break;
      case BusinessTypes.PIZZA_DELIVERY:
        view = this.localFacade.getClassInstance(PizzaDeliveryBusinessView);
        break;
      case BusinessTypes.DONATS_SHOP:
        view = this.localFacade.getClassInstance(DonatsShopBusinesView);
        break;
      case BusinessTypes.SHRIMP_BOAT:
        view = this.localFacade.getClassInstance(ShrimpBoatBusinessView);
        break;
      case BusinessTypes.HOCKEY_TEAM:
        view = this.localFacade.getClassInstance(HockeyTeamBusinessView);
        break;
      case BusinessTypes.MOVIE_STUDIO:
        view = this.localFacade.getClassInstance(MovieStudioBusinessView);
        break;
      case BusinessTypes.BANK:
        view = this.localFacade.getClassInstance(BankBusinessView);
        break;
      case BusinessTypes.OIL_COMPANY:
        view = this.localFacade.getClassInstance(OilCompanyBusinessView);
        break;
    }
    view.x = startPos.x;
    view.y = startPos.y;
    return view;
  }

  protected createModel(type: BusinessTypes): BusinessModel {
    const data: IPersistenceData = this.localFacade
      .getSingleton(CapitalistPersistenceManager)
      .load();
    const initValue: IBusinessParameters = data?.business[type]
      ? data?.business[type]
      : BusinessInitValues.get(type);
    return this.localFacade.getClassInstance(BusinessModel, type, initValue);
  }

  private createMediator(
    businessView: BusinessView,
    businessModel: BusinessModel
  ): BusinessViewMediator {
    return this.localFacade.getClassInstance(
      BusinessViewMediator,
      businessView,
      businessModel
    );
  }
}
