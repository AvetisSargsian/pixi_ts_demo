import {
  BusinessCostPrices,
  BusinessEvents,
  BusinessManagersCosts,
  BusinessModel,
  BusinessView,
} from "../..";
import {
  CompositeMediator,
  Facade,
  Image,
  TextField,
} from "../../../../frameworck";
import { Button } from "../../../../modules/ui";
import { ButtonState } from "../../../../modules/ui/views/ButtonState";
import { PayoutProgressBarMediator } from "./PayoutProgressBarMediator";

export class BusinessViewMediator extends CompositeMediator {
  protected nativeVIew: BusinessView;
  private localFacade: Facade = Facade.getSpace("CapitalistGameClone");
  private progressBar: PayoutProgressBarMediator;
  private upgradeBtn: Button;
  private managerBtn: Button;
  private launchBtn: Button;
  private grayPanel: Image;
  private orangePanel: Image;
  private profitTf: TextField;
  private costTf: TextField;
  private managerTf: TextField;

  constructor(view: BusinessView, private model: BusinessModel) {
    super(view);
    this.addInternalListeners();
  }

  public dispose(): void {
    this.model.dispose();
    this.model = null;
    super.dispose();
  }

  public update(balance: number) {
    if (!this.model.isEnabled) {
      const isEnough: boolean =
        balance >= BusinessCostPrices.get(this.model.type);
      this.orangePanel.visible = isEnough;
      this.grayPanel.visible = !isEnough;
    } else if (!this.model.haveManager) {
      const isEnough: boolean =
        balance >= BusinessManagersCosts.get(this.model.type);
      this.managerBtn.state = isEnough ? ButtonState.UP : ButtonState.DISABLED;
      this.managerBtn.interactive = isEnough;
      this.managerBtn.buttonMode = isEnough;
      this.managerTf.alpha = isEnough ? 1 : 0.5;
    }
  }

  protected onAddedToStage() {
    super.onAddedToStage();
    this.prepareView();
    if (this.model.isEnabled) {
      this.localFacade.dispatcher.emit(
        BusinessEvents.BUSINESS_ACTIVATE,
        this.model
      );
    }
  }

  protected addInternalListeners() {
    this.nativeVIew.on(
      BusinessView.UPGRADE_BUSINESS,
      this.onUpgradeRequest.bind(this)
    );
    this.nativeVIew.on(
      BusinessView.LAUNCH_BUSINESS,
      this.onBusinessLaunch.bind(this)
    );
    this.nativeVIew.on(
      BusinessView.HIRE_MANAGER,
      this.onHireManager.bind(this)
    );
    this.nativeVIew.on(
      BusinessView.ENABLE_BUSINESS_REQUEST,
      this.onEnableRequest.bind(this)
    );

    this.model.on(BusinessModel.ENABLED, this.onEnabled.bind(this));
    this.model.on(BusinessModel.TIMER_TICK, this.onTimerTick.bind(this));
    this.model.on(BusinessModel.MANAGER_HIRED, this.onManagerHired.bind(this));
    this.model.on(BusinessModel.UPGRADE, this.onUpgrade.bind(this));
  }

  private prepareView(): void {
    this.progressBar = this.localFacade.getClassInstance(
      PayoutProgressBarMediator
    );
    this.progressBar.addToParent(this.nativeVIew.mainLayer);
    this.add(this.progressBar);
    this.nativeVIew.mainLayer.setChildIndex(
      this.progressBar.getNativeVIew(),
      0
    );

    this.launchBtn = this.nativeVIew.mainLayer.getChildByName(
      "LAUNCH_BTN"
    ) as Button;
    this.managerBtn = this.nativeVIew.mainLayer.getChildByName(
      "MANAGER_BTN"
    ) as Button;
    this.upgradeBtn = this.nativeVIew.mainLayer.getChildByName(
      "UPGRADE_BTN"
    ) as Button;
    this.profitTf = this.nativeVIew.mainLayer.getChildByName(
      "PROFIT_TF"
    ) as TextField;
    this.managerTf = this.nativeVIew.mainLayer.getChildByName(
      "MANAGER_TF"
    ) as TextField;
    this.nativeVIew.mainLayer.visible = this.model.isEnabled;

    this.grayPanel = this.nativeVIew.startLayer.getChildByName(
      "GREY_PANEL"
    ) as Image;
    this.orangePanel = this.nativeVIew.startLayer.getChildByName(
      "ORANGE_PANEL"
    ) as Image;
    this.costTf = this.nativeVIew.startLayer.getChildByName(
      "COST_TF"
    ) as TextField;
    this.nativeVIew.startLayer.visible = !this.model.isEnabled;
    this.orangePanel.visible = false;
    this.grayPanel.visible = true;

    this.costTf.text = `Cost ${BusinessCostPrices.get(this.model.type)}`;
    this.managerTf.text = `Cost ${BusinessManagersCosts.get(this.model.type)}`;

    this.onUpgrade();
    if (this.model.haveManager) {
      this.onManagerHired();
    }
    if (this.model.isEnabled) {
      this.onEnabled();
    }
  }

  private onUpgrade(): void {
    this.profitTf.text = `Profit ${this.model
      .getParameters()
      .payOutValue.toFixed(2)}`;
  }

  private onEnabled() {
    this.nativeVIew.mainLayer.visible = this.model.isEnabled;
    this.nativeVIew.startLayer.visible = !this.model.isEnabled;
  }

  private onManagerHired(): void {
    this.managerBtn.visible = false;
    this.managerTf.visible = false;

    this.launchBtn.state = ButtonState.DISABLED;
    this.launchBtn.interactive = false;
    this.launchBtn.buttonMode = false;
  }

  private onTimerTick(): void {
    this.launchBtn.state =
      this.model.progress < 100 || this.model.haveManager
        ? ButtonState.DISABLED
        : ButtonState.UP;
    this.progressBar.setProgress(this.model.progress);
  }

  private onUpgradeRequest(): void {
    this.localFacade.dispatcher.emit(
      BusinessEvents.BUSINESS_UPGRADE,
      this.model
    );
  }

  private onBusinessLaunch(): void {
    this.localFacade.dispatcher.emit(
      BusinessEvents.BUSINESS_LAUNCH,
      this.model
    );
  }

  private onHireManager(): void {
    this.localFacade.dispatcher.emit(
      BusinessEvents.BUSINESS_HIRE_MANAGER,
      this.model
    );
  }

  private onEnableRequest() {
    this.localFacade.dispatcher.emit(
      BusinessEvents.BUSINESS_PURCHASE,
      this.model
    );
  }
}
