import { Container } from "pixi.js";
import { BaseView, ComponentMediator, Facade } from "../../../../frameworck";
import { PayoutProgressBarView } from "../view/PayoutProgressBarView";

export class PayoutProgressBarMediator extends ComponentMediator {
  protected nativeVIew: PayoutProgressBarView;
  private barLength: number;

  public setProgress(value: number): void {
    value = Math.min(Math.max(value, 0), 100);
    (this.nativeVIew.mask as Container).x =
      this.barLength * value - this.barLength;
  }

  protected onAddedToStage() {
    super.onAddedToStage();
    this.initProgressBar();
  }

  protected setNativeVIew(): BaseView {
    return Facade.getSpace("CapitalistGameClone").getClassInstance(
      PayoutProgressBarView
    );
  }

  private initProgressBar(): void {
    this.barLength = this.nativeVIew.width;
  }
}
