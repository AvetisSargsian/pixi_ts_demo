import { BusinessTypes } from "./BusinessTypes";

export const BusinessManagersCosts: Map<BusinessTypes, number> = new Map<
  BusinessTypes,
  number
>([
  [BusinessTypes.LEMONADE, 500],
  [BusinessTypes.NEWSPAPER, 3000],
  [BusinessTypes.CAR_WASH, 4000],
  [BusinessTypes.PIZZA_DELIVERY, 7000],
  [BusinessTypes.DONATS_SHOP, 11000],
  [BusinessTypes.SHRIMP_BOAT, 18000],
  [BusinessTypes.HOCKEY_TEAM, 25000],
  [BusinessTypes.MOVIE_STUDIO, 33000],
  [BusinessTypes.BANK, 58000],
  [BusinessTypes.OIL_COMPANY, 91000],
]);
