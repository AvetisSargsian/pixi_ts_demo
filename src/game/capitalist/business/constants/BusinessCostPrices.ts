import { BusinessTypes } from "./BusinessTypes";

export const BusinessCostPrices: Map<BusinessTypes, number> = new Map<
  BusinessTypes,
  number
>([
  [BusinessTypes.LEMONADE, 0],
  [BusinessTypes.NEWSPAPER, 300],
  [BusinessTypes.CAR_WASH, 400],
  [BusinessTypes.PIZZA_DELIVERY, 700],
  [BusinessTypes.DONATS_SHOP, 1100],
  [BusinessTypes.SHRIMP_BOAT, 1800],
  [BusinessTypes.HOCKEY_TEAM, 2500],
  [BusinessTypes.MOVIE_STUDIO, 3300],
  [BusinessTypes.BANK, 5800],
  [BusinessTypes.OIL_COMPANY, 9100],
]);
