export const enum BusinessTypes {
  LEMONADE = "BusinessTypes.LEMONADE",
  NEWSPAPER = "BusinessTypes.NEWSPAPER",
  CAR_WASH = "BusinessTypes.CARWASH",
  PIZZA_DELIVERY = "BusinessTypes.PIZZA_DELIVERY",
  DONATS_SHOP = "BusinessTypes.DONATS_SHOP",
  SHRIMP_BOAT = "BusinessTypes.SHRIMP_BOAT",
  HOCKEY_TEAM = "BusinessTypes.HOCKEY_TEAM",
  MOVIE_STUDIO = "BusinessTypes.MOVIE_STUDIO",
  BANK = "BusinessTypes.BANK",
  OIL_COMPANY = "BusinessTypes.OIL_COMPANY",
}
