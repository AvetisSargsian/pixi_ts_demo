import { IBusinessParameters } from "../../interfaces/IBusinessParameters";
import { BusinessTypes } from "./BusinessTypes";

export const BusinessInitValues: Map<BusinessTypes, IBusinessParameters> =
  new Map<BusinessTypes, IBusinessParameters>([
    [
      BusinessTypes.LEMONADE,
      {
        payOutTime: 1000,
        payOutValue: 50,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: true,
      },
    ],
    [
      BusinessTypes.NEWSPAPER,
      {
        payOutTime: 5000,
        payOutValue: 100,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
    [
      BusinessTypes.CAR_WASH,
      {
        payOutTime: 10000,
        payOutValue: 150,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
    [
      BusinessTypes.PIZZA_DELIVERY,
      {
        payOutTime: 15000,
        payOutValue: 250,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
    [
      BusinessTypes.DONATS_SHOP,
      {
        payOutTime: 25000,
        payOutValue: 400,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
    [
      BusinessTypes.SHRIMP_BOAT,
      {
        payOutTime: 40000,
        payOutValue: 650,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
    [
      BusinessTypes.HOCKEY_TEAM,
      {
        payOutTime: 65000,
        payOutValue: 1050,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
    [
      BusinessTypes.MOVIE_STUDIO,
      {
        payOutTime: 105000,
        payOutValue: 1700,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
    [
      BusinessTypes.BANK,
      {
        payOutTime: 170000,
        payOutValue: 2750,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
    [
      BusinessTypes.OIL_COMPANY,
      {
        payOutTime: 275000,
        payOutValue: 4450,
        upgradeLevel: 0,
        manager: false,
        launchTime: 0,
        isEnabled: false,
      },
    ],
  ]);
