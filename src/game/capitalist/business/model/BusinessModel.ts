import { BusinessTypes } from "../..";
import { Model } from "../../../../frameworck";
import { IBusinessParameters } from "../../interfaces/IBusinessParameters";

export class BusinessModel extends Model {
  public static readonly TIMER_START: string = "BusinessModel.TIMER_START";
  public static readonly TIMER_TICK: string = "BusinessModel.TIMER_TICK";
  public static readonly TIMER_END: string = "BusinessModel.TIMER_END";
  public static readonly MANAGER_HIRED: string = "BusinessModel.MANAGER_HIRED";
  public static readonly ENABLED: string = "BusinessModel.ENABLED";
  public static readonly UPGRADE: string = "BusinessModel.UPGRADE";

  private _payOutTime: number = 0;
  private _payOutValue: number = 0;
  private _upgradeLevel: number = 0;

  private _manager: boolean;
  private _deposit: number = 0;
  private _isEnabled: boolean = false;
  private _launchTime: number = 0;
  private _progress: number = 0;

  constructor(private _type: BusinessTypes, initValue: IBusinessParameters) {
    super();

    this._payOutTime = initValue.payOutTime;
    this._payOutValue = initValue.payOutValue;
    this._upgradeLevel = initValue.upgradeLevel;
    this._launchTime = initValue.launchTime;
    this._manager = initValue.manager;
    this._isEnabled = initValue.isEnabled;
  }

  public dispose() {
    this.resetTimer();
    super.dispose();
  }

  public getParameters(): IBusinessParameters {
    return {
      payOutTime: this._payOutTime,
      payOutValue: this._payOutValue,
      upgradeLevel: this._upgradeLevel,
      launchTime: this._launchTime,
      manager: this._manager,
      isEnabled: this._isEnabled,
    };
  }

  public upgrade(): void {
    this._upgradeLevel++;
    this._payOutTime -= this._upgradeLevel / this._payOutTime;
    this._payOutValue += (2 * this._upgradeLevel) / this._payOutValue;
    this.emit(BusinessModel.UPGRADE);
  }

  public launch(): void {
    if (!this._isEnabled || this._launchTime !== 0) {
      return;
    }
    this.emit(BusinessModel.TIMER_START);
    this._launchTime = Date.now();
  }

  public update(time): void {
    if (this._launchTime === 0) {
      return;
    }
    const delta: number = time - this._launchTime;
    if (!this._manager) {
      if (delta >= this._payOutTime) {
        this._deposit = this._payOutValue;
        this.resetTimer();
      } else {
        this._progress = delta / this._payOutTime;
      }
    } else {
      this._deposit = Math.floor(delta / this._payOutTime) * this._payOutValue;
      this._progress = (delta % this._payOutTime) / this._payOutTime;
      if (this._deposit > 0) {
        this._launchTime = Date.now() - this._progress * this._payOutTime;
      }
    }
    this.emit(BusinessModel.TIMER_TICK);
  }

  public withdrewDeposit(): number {
    const result = this._deposit;
    this._deposit = 0;
    return result;
  }

  public get deposit(): number {
    return this._deposit;
  }

  public get progress(): number {
    return this._progress;
  }

  public get haveManager(): boolean {
    return this._manager;
  }

  public hireManager(): void {
    this._manager = true;
    this.emit(BusinessModel.MANAGER_HIRED);
  }

  public get launchTime(): number {
    return this._launchTime;
  }

  public enable(): void {
    this._isEnabled = true;
    this.emit(BusinessModel.ENABLED);
  }

  public disable(): void {
    this._isEnabled = false;
  }

  public get isEnabled(): boolean {
    return this._isEnabled;
  }

  public get type(): BusinessTypes {
    return this._type;
  }

  public tryAutoLaunch() {
    if (this._manager) {
      this.launch();
    }
  }

  private increaseTimer(value: number = 1): void {
    this._launchTime += value;
    if (this._launchTime >= this._payOutTime) {
      this._launchTime = this._payOutTime;
      this.emit(BusinessModel.TIMER_END);
    }
  }

  private resetTimer(): void {
    this._progress = 1;
    this._launchTime = 0;
  }
}
