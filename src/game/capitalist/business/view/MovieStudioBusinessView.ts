import { BusinessView } from "./BusinessView";

export class MovieStudioBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "MOVIE STUDIO";
    this.tf.y = -35;
    this.tf.x = 40;
  }
}
