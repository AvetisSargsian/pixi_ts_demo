import {
  Assets,
  BaseView,
  Facade,
  Image,
  TextField,
  TextStyle,
} from "../../../../frameworck";
import { Button, ButtonProperties, UIFactory } from "../../../../modules/ui";

export class BusinessView extends BaseView {
  public static readonly UPGRADE_BUSINESS: string =
    "BusinessView.UPGRADE_BUSINESS";
  public static readonly LAUNCH_BUSINESS: string =
    "BusinessView.LAUNCH_BUSINESS";
  public static readonly HIRE_MANAGER: string = "BusinessView.HIRE_MANAGER";
  public static readonly ENABLE_BUSINESS_REQUEST: string =
    "BusinessView.ENABLE_BUSINESS_REQUEST";

  public startLayer: BaseView;
  public mainLayer: BaseView;
  protected tf: TextField;
  private uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);

  private readonly textStyle = new TextStyle({
    fontSize: 30,
    fill: "#ff0000",
    align: "center",
  });

  private readonly textStyle1 = new TextStyle({
    fontSize: 30,
    fill: "#ffffff",
    align: "center",
  });

  protected onAdded() {
    super.onAdded();

    this.mainLayer = new BaseView();
    this.addChild(this.mainLayer);

    this.startLayer = new BaseView();
    this.addChild(this.startLayer);

    const gray_panel: Image = new Image(
      this.resources[Assets.load.gray_panel].texture
    );
    gray_panel.name = "GREY_PANEL";
    this.startLayer.addChild(gray_panel);

    const orange_panel: Image = new Image(
      this.resources[Assets.load.orange_panel].texture
    );
    orange_panel.name = "ORANGE_PANEL";
    this.startLayer.addChild(orange_panel);

    orange_panel.interactive = true;
    orange_panel.buttonMode = true;
    orange_panel.addListener("pointerdown", () =>
      this.emit(BusinessView.ENABLE_BUSINESS_REQUEST)
    );

    const costTf = new TextField("", this.textStyle1);
    costTf.y = 15;
    costTf.x = 50;
    costTf.name = "COST_TF";
    this.startLayer.addChild(costTf);

    this.tf = new TextField("BASE VIEW", this.textStyle);
    this.addChild(this.tf);

    const tf1 = new TextField("", this.textStyle1);
    tf1.y = 4;
    tf1.x = 40;
    tf1.name = "PROFIT_TF";
    this.mainLayer.addChild(tf1);

    const managerBtn: Button = this.uiFactory.createUiButton({
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 100,
        height: 50,
      }),
      text: "HIRE Manager",
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 100,
        height: 50,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 100,
        height: 50,
      }),
      callBack: () => this.emit(BusinessView.HIRE_MANAGER),
    } as ButtonProperties);

    managerBtn.x = 270;
    managerBtn.y = 0;
    managerBtn.name = "MANAGER_BTN";
    this.mainLayer.addChild(managerBtn);

    const managerCostTf = new TextField("", this.textStyle1);
    managerCostTf.y = 40;
    managerCostTf.x = 260;
    managerCostTf.name = "MANAGER_TF";
    this.mainLayer.addChild(managerCostTf);

    const upgradeBtn: Button = this.uiFactory.createUiButton({
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 115,
        height: 40,
      }),
      text: "UPGRADE",
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 115,
        height: 40,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 115,
        height: 40,
      }),
      callBack: () => this.emit(BusinessView.UPGRADE_BUSINESS),
    } as ButtonProperties);
    upgradeBtn.y = 50;
    upgradeBtn.x = 120;
    upgradeBtn.name = "UPGRADE_BTN";
    this.mainLayer.addChild(upgradeBtn);

    const launchBtn: Button = this.uiFactory.createUiButton({
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 115,
        height: 40,
      }),
      text: "LAUNCH",
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 115,
        height: 40,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 115,
        height: 40,
      }),
      callBack: () => this.emit(BusinessView.LAUNCH_BUSINESS),
    } as ButtonProperties);

    launchBtn.y = 50;
    launchBtn.name = "LAUNCH_BTN";
    this.mainLayer.addChild(launchBtn);
  }
}
