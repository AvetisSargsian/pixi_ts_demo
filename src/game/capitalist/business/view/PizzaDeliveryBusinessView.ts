import { BusinessView } from "./BusinessView";

export class PizzaDeliveryBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "PIZZA DELIVERY";
    this.tf.y = -35;
    this.tf.x = 0;
  }
}
