import { BusinessView } from "./BusinessView";

export class LemonadeBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "LEMONADE";
    this.tf.y = -35;
    this.tf.x = 40;
  }
}
