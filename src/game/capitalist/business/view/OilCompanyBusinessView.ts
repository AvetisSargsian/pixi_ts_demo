import { BusinessView } from "./BusinessView";

export class OilCompanyBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "OIL COMPANY";
    this.tf.y = -35;
    this.tf.x = 40;
  }
}
