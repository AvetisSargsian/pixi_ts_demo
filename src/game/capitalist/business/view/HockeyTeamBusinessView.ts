import { BusinessView } from "./BusinessView";

export class HockeyTeamBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "HOKEY TEAM";
    this.tf.y = -35;
    this.tf.x = 40;
  }
}
