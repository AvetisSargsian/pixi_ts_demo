import { BusinessView } from "./BusinessView";

export class BankBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "BANK";
    this.tf.y = -35;
    this.tf.x = 80;
  }
}
