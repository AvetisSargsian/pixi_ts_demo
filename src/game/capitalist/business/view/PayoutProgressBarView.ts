import { Assets, BaseView, Graphics, Image } from "../../../../frameworck";

export class PayoutProgressBarView extends BaseView {
  public static readonly PROGRESS_BAR_NAME: string = "PayoutProgressBar";

  public onAdded() {
    super.onAdded();
    const progressBar: Image = new Image(
      this.resources[Assets.load.progress_bar].texture
    );
    progressBar.name = PayoutProgressBarView.PROGRESS_BAR_NAME;
    // progressBar.tint =  0xffffff * 0.8;
    this.addChild(progressBar);

    const progressBarMask = new Graphics();
    progressBarMask.beginFill(0, 0.2);
    progressBarMask.drawRect(0, 0, progressBar.width, progressBar.height);
    progressBarMask.endFill();

    this.mask = progressBarMask;
    this.addChild(progressBarMask);
  }
}
