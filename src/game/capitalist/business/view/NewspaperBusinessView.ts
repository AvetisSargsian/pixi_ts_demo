import { BusinessView } from "./BusinessView";

export class NewspaperBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "NEWSPAPER";
    this.tf.y = -35;
    this.tf.x = 20;
  }
}
