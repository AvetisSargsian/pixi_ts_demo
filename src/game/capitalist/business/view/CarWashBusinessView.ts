import { BusinessView } from "./BusinessView";

export class CarWashBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "CAR WASH";
    this.tf.y = -35;
    this.tf.x = 40;
  }
}
