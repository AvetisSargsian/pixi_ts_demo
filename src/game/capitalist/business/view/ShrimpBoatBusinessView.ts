import { BusinessView } from "./BusinessView";

export class ShrimpBoatBusinessView extends BusinessView {
  protected onAdded() {
    super.onAdded();
    this.tf.text = "SRIMP BOAT";
    this.tf.y = -35;
    this.tf.x = 40;
  }
}
