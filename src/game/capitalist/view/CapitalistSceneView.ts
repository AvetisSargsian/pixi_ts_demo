import {
  Application,
  BaseView,
  Facade,
  TextField,
  TextStyle,
} from "../../../frameworck";
import { Button, ButtonProperties, UIFactory } from "../../../modules/ui";
import { ScenesList } from "../../navigation/ScenesList";
import { CapitalistInternalEvents } from "../events/CapitalistInternalEvents";

export class CapitalistSceneView extends BaseView {
  public static readonly DEPOSIT_TEXT: string =
    "CapitalistSceneView.DEPOSIT_TEXT";
  private uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);

  protected onAdded() {
    super.onAdded();

    const textStyle = new TextStyle({
      fontSize: 45,
      fill: "#0000ff",
      align: "center",
    });
    const tf = new TextField("0000000", textStyle);
    tf.name = CapitalistSceneView.DEPOSIT_TEXT;
    tf.y = 300;
    tf.x = 80;
    this.addChild(tf);

    const tf1 = new TextField("BALANCE", textStyle);
    tf1.y = 250;
    tf1.x = 45;
    this.addChild(tf1);

    const backBtn: Button = this.uiFactory.createUiButton({
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      text: "Main Menu",
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () =>
        this.emit(
          CapitalistInternalEvents.GO_BACK_BUTTON_PRESSED,
          ScenesList.MENU
        ),
    } as ButtonProperties);

    backBtn.y = Application.GAME_HEIGHT - backBtn.height;
    this.addChild(backBtn);
  }
}
