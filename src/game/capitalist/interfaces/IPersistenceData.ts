import { BusinessTypes } from "..";
import { IBusinessParameters } from "./IBusinessParameters";

export interface IPersistenceData {
  mainBalance: number;
  business: { [key in BusinessTypes]: IBusinessParameters };
}
