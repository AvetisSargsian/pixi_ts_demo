export interface IBusinessParameters {
  payOutTime: number;
  payOutValue: number;
  upgradeLevel: number;
  manager: boolean;
  isEnabled: boolean;
  launchTime: number;
}
