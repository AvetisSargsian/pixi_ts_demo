import { Point } from "pixi.js"; // remove pixi dependency from game
import {
  Application,
  Assets,
  BaseView,
  Facade,
  Image,
  Utils,
} from "../../../frameworck";
import { Button, ButtonProperties, UIFactory } from "../../../modules/ui";
import { ScenesList } from "../../navigation/ScenesList";

export class CardsScene extends BaseView {
  public static readonly BUTTON_PRESSED: string = "CardsScene.BUTTON_PRESSED";

  private readonly id = this.resources[Assets.load.cards].textures;
  private deck: Image[] = [];
  private _cardsLayout: BaseView;
  private uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);

  public constructor() {
    super();
    this._cardsLayout = new BaseView();
  }

  public get cardsLayout(): BaseView {
    return this._cardsLayout;
  }

  public createCard(name: string, position: Point) {
    if (this.id) {
      const img: Image = new Image(this.id[name]);
      img.name = name;
      img.x = position.x;
      img.y = position.y;
      this.deck.push(img);
      this._cardsLayout.addChild(img);
    }
  }

  public shiftCard(index: number, point: Point): Promise<void> {
    return new Promise<void>((resolve) => {
      const card: Image = this.deck.splice(index, 1)[0];
      if (card) {
        this.deck.unshift(card);
        Utils.tweenTo(card, 0.7, {
          x: point.x - card.width,
          y: point.y,
          onComplete: () => {
            this._cardsLayout.addChildAt(card, 0);
            const time = 300;
            this.moveOthers(time);
            Utils.setTimeout(resolve, time);
          },
        });
      } else {
        resolve();
      }
    });
  }

  public moveOthers(time: number) {
    time = time / 1000;
    this.deck.forEach((value, index) => {
      if (index > 0) {
        Utils.tweenTo(value, time, {
          x: value.x + 15,
        });
      } else {
        Utils.tweenTo(value, time, {
          x: value.x + value.width,
        });
      }
    });
  }

  protected onAdded() {
    super.onAdded();

    this._cardsLayout = new BaseView();
    this.addChild(this._cardsLayout);

    const backBtn: Button = this.uiFactory.createUiButton({
      text: "Main Menu",
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () => this.emit(CardsScene.BUTTON_PRESSED, ScenesList.MENU),
    } as ButtonProperties);
    backBtn.y = Application.GAME_HEIGHT - backBtn.height;
    this.addChild(backBtn);
  }
}
