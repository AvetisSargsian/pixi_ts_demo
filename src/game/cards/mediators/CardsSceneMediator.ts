import { Point } from "pixi.js";
import {
  BaseView,
  NavigationIntentsApi,
  SceneMediator,
} from "../../../frameworck";
import { CardsDeck } from "../constance";
import { CardsScene } from "../view/CardsScene";

export class CardsSceneMediator extends SceneMediator {
  protected nativeVIew!: CardsScene;

  protected readonly startPoint = new Point(150, 150);
  protected cards!: string[];

  protected onAddedToStage(): void {
    super.onAddedToStage();

    this.setupCards();
    this.nativeVIew.cardsLayout.buttonMode = true;
    this.nativeVIew.cardsLayout.interactive = true;

    // const blur: PIXI.filters.BlurFilter = new PIXI.filters.BlurFilter();
    // blur.autoFit = true;
    // this.nativeVIew.cardsLayout.filters = [blur];

    this.nativeVIew.cardsLayout.addListener("pointerdown", this.onClick, this);
    this.nativeVIew.on(CardsScene.BUTTON_PRESSED, this.onButtonPressed, this);
  }

  protected onButtonPressed(btnId: number): void {
    this.nativeVIew.cardsLayout.removeListener(
      "pointerdown",
      this.onClick,
      this
    );
    this.nativeVIew.off(CardsScene.BUTTON_PRESSED, this.onButtonPressed);
    NavigationIntentsApi.EMIT_CHANGE_SCENE_TO(btnId);
  }

  protected setNativeVIew(): BaseView {
    return new CardsScene();
  }

  private setupCards(): void {
    this.cards = CardsDeck.concat();
    this.shuffle();

    const point = this.startPoint.clone();
    this.cards.forEach((value: string) => {
      this.nativeVIew.createCard(value, point);
      point.x += 15;
    });
  }

  private shuffle(): void {
    for (let i = 0, count = this.cards.length; i < count; i++) {
      const randomIndex = Math.round(Math.random() * count - 1);

      const temp = this.cards[i];
      this.cards[i] = this.cards[randomIndex];
      this.cards[randomIndex] = temp;
    }
  }

  private onClick(): void {
    const temp = this.cards.pop();
    if (temp) {
      this.cards.unshift(temp);

      this.nativeVIew.cardsLayout.interactive = false;
      this.nativeVIew
        .shiftCard(this.cards.length - 1, this.startPoint)
        .then(() => (this.nativeVIew.cardsLayout.interactive = true));
    }
  }
}
