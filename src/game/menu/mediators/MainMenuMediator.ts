import {
  BaseView,
  NavigationIntentsApi,
  SceneMediator,
} from "../../../frameworck";
import { MainMenuScene } from "../views/MainMenuScene";

export class MainMenuMediator extends SceneMediator {
  public dispose(): void {
    this.nativeVIew.off(
      MainMenuScene.BUTTON_PRESSED,
      this.onButtonPressed,
      this
    );
    super.dispose();
  }

  protected onAddedToStage(): void {
    super.onAddedToStage();
    this.nativeVIew.on(
      MainMenuScene.BUTTON_PRESSED,
      this.onButtonPressed,
      this
    );
  }

  protected setNativeVIew(): BaseView {
    return new MainMenuScene();
  }

  protected onButtonPressed(btnId: number) {
    NavigationIntentsApi.EMIT_CHANGE_SCENE_TO(btnId);
  }
}
