import { BaseView, Facade } from "../../../frameworck";
import { ButtonProperties, UIFactory } from "../../../modules/ui";
import { ScenesList } from "../../navigation/ScenesList";

export class MainMenuScene extends BaseView {
  public static readonly BUTTON_PRESSED: string =
    "MainMenuScene.BUTTON_PRESSED";
  private uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);
  protected onAdded() {
    super.onAdded();

    const cards = this.uiFactory.createUiButton({
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      text: "Play Cards",
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () => this.emit(MainMenuScene.BUTTON_PRESSED, ScenesList.CARDS),
    } as ButtonProperties);
    cards.x = 300;
    cards.y = 150;
    this.addChild(cards);

    const particles = this.uiFactory.createUiButton({
      text: "Show Particles",
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () =>
        this.emit(MainMenuScene.BUTTON_PRESSED, ScenesList.PARTICLES),
    } as ButtonProperties);
    particles.x = 600;
    particles.y = 150;
    this.addChild(particles);

    const slots = this.uiFactory.createUiButton({
      text: "Slots",
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () =>
        this.emit(MainMenuScene.BUTTON_PRESSED, ScenesList.REELS_SCENE),
    } as ButtonProperties);
    slots.x = 300;
    slots.y = 250;
    this.addChild(slots);

    const tanks = this.uiFactory.createUiButton({
      text: "Tanks",
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () => this.emit(MainMenuScene.BUTTON_PRESSED, ScenesList.TANKS),
    } as ButtonProperties);
    tanks.x = 600;
    tanks.y = 250;
    this.addChild(tanks);

    const capitalist = this.uiFactory.createUiButton({
      text: "Capitalist",
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () =>
        this.emit(MainMenuScene.BUTTON_PRESSED, ScenesList.CAPITALIST),
    } as ButtonProperties);
    capitalist.x = 850;
    capitalist.y = 150;
    this.addChild(capitalist);
  }
}
