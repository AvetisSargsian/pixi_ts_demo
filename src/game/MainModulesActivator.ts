import { Assets, IModuleActivator } from "../frameworck";
import { ConfigsActivator } from "../modules/config";
import { LayoutActivator } from "../modules/layouts";
import { LoaderActivator } from "../modules/loader";
import { NavigationActivator } from "../modules/navigation";
import { UIActivator } from "../modules/ui";
import { CommonRulesActivatorApp } from "./commonrules/CommonRulesActivatorApp";
import { GameAssets } from "./GameAssets";
import { NavigationActivatorApp } from "./navigation/NavigationActivatorApp";

export class MainModuleActivator implements IModuleActivator {
  public activate() {
    // if (__MOBILE__) {}
    // if (__DESKTOP__) {}
    // if (__PROD__) {}

    // MERGE GAME ASSETS
    Object.assign(Assets, GameAssets);

    // Global modules
    new LayoutActivator().activate();
    new ConfigsActivator().activate();
    new NavigationActivator().activate();
    new LoaderActivator().activate();
    new UIActivator().activate();
    new NavigationActivatorApp().activate();
    new CommonRulesActivatorApp().activate();
  }
}
