import {
  Application,
  Facade,
  IModuleActivator,
  LoaderEvents,
} from "../../frameworck";

import { LoadingCompleteAction } from "./actions/LoadingCompleteAction";
import { OnApplicationReadyAction } from "./actions/OnApplicationReadyAction";
import { PrepareAssetsAction } from "./actions/PrepareAssetsAction";

export class CommonRulesActivatorApp implements IModuleActivator {
  public activate(data?: any): void {
    Facade.global.bindCommandToEvent(
      Application.READY,
      OnApplicationReadyAction
    );
    Facade.global.bindCommandToEvent(LoaderEvents.LOADING_COMPLETE, [
      PrepareAssetsAction,
      LoadingCompleteAction,
    ]);
  }
}
