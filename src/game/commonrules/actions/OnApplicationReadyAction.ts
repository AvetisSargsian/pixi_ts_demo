import { BaseCommand, NavigationIntentsApi } from "../../../frameworck";
import { ScenesList } from "../../navigation/ScenesList";

export class OnApplicationReadyAction extends BaseCommand {
  public execute(data?: any): void {
    NavigationIntentsApi.EMIT_CHANGE_SCENE_TO(ScenesList.LOADING);
  }
}
