import { BaseCommand, NavigationIntentsApi, Utils } from "../../../frameworck";
import { ScenesList } from "../../navigation/ScenesList";

export class LoadingCompleteAction extends BaseCommand {
  private timeOutId: number;

  public execute(data?: any): void {
    this.timeOutId = Utils.setTimeout(() => this.onTimeOut(), 2000);
  }

  private onTimeOut(): void {
    Utils.clearTimeout(this.timeOutId);
    NavigationIntentsApi.EMIT_CHANGE_SCENE_TO(ScenesList.MENU);
  }
}
