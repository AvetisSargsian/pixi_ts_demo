import { Application, Assets, BaseCommand } from "../../../frameworck";
import { Texture, utils } from "pixi.js";

export class PrepareAssetsAction extends BaseCommand {
  public execute(data?: any): void {
    const { TextureCache } = utils;
    const renderer = Application.RENDERER();

    Object.values(Assets.load).forEach((textureKey) => {
      let texture: Texture =
        TextureCache[textureKey] || TextureCache[`${textureKey}_image`];
      if (texture?.baseTexture) {
        if (!texture.baseTexture._glTextures[renderer.CONTEXT_UID]) {
          renderer.texture.bind(texture.baseTexture);
        }
      }
    });
  }
}
