export const enum ScenesList {
  LOADING,
  MENU,
  CARDS,
  PARTICLES,
  REELS_SCENE,
  TANKS,
  CAPITALIST,
}
