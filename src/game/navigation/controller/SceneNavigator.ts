import { ClassConstructor, Facade, SceneMediator } from "../../../frameworck";
import { LoaderMediator } from "../../../modules/loader";
import { NavigationController } from "../../../modules/navigation";
import { CapitalistActivator, CapitalistSceneMediator } from "../../capitalist";
import { CardsSceneMediator } from "../../cards/mediators/CardsSceneMediator";
import { MainMenuMediator } from "../../menu/mediators/MainMenuMediator";
import { ParticlesSceneMediator, ParticlesActivator } from "../../particles";
import { ReelsActivator, ReelsSceneMediator } from "../../reelsGame";
import { TanksActivator, TanksSceneMediator } from "../../tanks";
import { ScenesList } from "../ScenesList";

export class SceneNavigator extends NavigationController {
  protected changeSceneTo(sceneId: ScenesList) {
    switch (sceneId) {
      case ScenesList.LOADING: {
        this.switchScene(LoaderMediator);
        break;
      }
      case ScenesList.MENU: {
        // TODO:: refactor to use "switchScene" method
        this.model.currentSceneMediator = new MainMenuMediator();
        break;
      }
      case ScenesList.CARDS: {
        // TODO:: refactor to use "switchScene" method
        this.model.currentSceneMediator = new CardsSceneMediator();
        break;
      }
      case ScenesList.PARTICLES: {
        Facade.removeNameSpace("ParticlesGame");
        new ParticlesActivator().activate();
        this.switchScene(ParticlesSceneMediator, "ParticlesGame");
        break;
      }
      case ScenesList.REELS_SCENE: {
        Facade.removeNameSpace("ReelsGame");
        new ReelsActivator().activate();
        this.switchScene(ReelsSceneMediator, "ReelsGame");
        break;
      }
      case ScenesList.TANKS: {
        Facade.removeNameSpace("TanksGame");
        new TanksActivator().activate();
        this.switchScene(TanksSceneMediator, "TanksGame");
        break;
      }
      case ScenesList.CAPITALIST: {
        Facade.removeNameSpace("CapitalistGameClone");
        new CapitalistActivator().activate();
        this.switchScene(CapitalistSceneMediator, "CapitalistGameClone");
        break;
      }
    }
  }

  protected switchScene<T extends SceneMediator>(
    scene: ClassConstructor<T>,
    spaceName?: string
  ): void {
    if (spaceName) {
      this.model.currentSceneMediator =
        Facade.getSpace(spaceName).getClassInstance(scene);
    } else {
      this.model.currentSceneMediator = Facade.global.getClassInstance(scene);
    }
  }
}
