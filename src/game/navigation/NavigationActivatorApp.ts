import { Facade, IModuleActivator } from "../../frameworck";
import { NavigationController } from "../../modules/navigation";
import { SceneNavigator } from "./controller/SceneNavigator";

export class NavigationActivatorApp implements IModuleActivator {
  public activate(data?: any): void {
    Facade.global.registerSingletonAs(NavigationController, SceneNavigator);
  }
}
