import { TanksInternalEvents } from "../.";
import { Application, BaseView, Facade } from "../../../frameworck";
import { Button, ButtonProperties, UIFactory } from "../../../modules/ui";
import { ScenesList } from "../../navigation/ScenesList";

export class TanksSceneView extends BaseView {
  private uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);

  protected onAdded() {
    super.onAdded();

    const backBtn: Button = this.uiFactory.createUiButton({
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      text: "Main Menu",
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () =>
        this.emit(TanksInternalEvents.GO_BACK_BUTTON_PRESSED, ScenesList.MENU),
    } as ButtonProperties);

    backBtn.y = Application.GAME_HEIGHT - backBtn.height;
    this.addChild(backBtn);
  }
}
