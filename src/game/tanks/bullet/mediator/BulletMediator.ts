import { ComponentMediator, Facade } from "../../../../frameworck";
import { BulletEvents } from "../event/BulletEvent";
import { BulletModel } from "../model/BulletModel";
import { BulletView } from "../view/BulletView";

export class BulletMediator extends ComponentMediator {
  constructor(private model: BulletModel) {
    super();
    this.model.on(BulletEvents.MOVE, this.updatePos, this);
    this.model.on(BulletEvents.HIT, this.destroyBullet, this);
  }

  public dispose(): void {
    this.model.dispose();
    super.dispose();
  }

  protected destroyBullet(): void {
    this.dispose();
  }

  protected setNativeVIew(): BulletView {
    const localFacade: Facade = Facade.getSpace("TanksGame");
    return localFacade.getClassInstance(BulletView);
  }

  protected onAddedToStage(): void {
    super.onAddedToStage();
    this.nativeVIew.position = this.model.getCurrentPos();
  }

  private updatePos(): void {
    const pos = this.model.getCurrentPos();
    this.nativeVIew.x = pos.x;
    this.nativeVIew.y = pos.y;
  }
}
