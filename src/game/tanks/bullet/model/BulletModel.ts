import { Point } from "pixi.js";
import { BulletEvents, ITile, LevelModel } from "../..";
import { Facade, Model } from "../../../../frameworck";

export class BulletModel extends Model {
  public readonly speed: number = 10;
  public readonly distance = 600;
  private readonly toRadians: number = Math.PI / 180;
  private position: Point;
  private dead: boolean = false;
  private obstacles: ITile[];

  constructor(
    private origin: Point,
    private direction: number,
    private damage: number
  ) {
    super();
    this.position = this.origin.clone();
  }

  public isDead(): boolean {
    return this.dead;
  }

  public setObstacles(walls: ITile[]): void {
    this.obstacles = walls;
  }

  public dispose(): void {
    this.position = null;
    super.dispose();
  }

  public destroy(): void {
    this.dead = true;
    this.emit(BulletEvents.HIT);
  }

  public getCurrentPos(): Point {
    return this.position;
  }

  public getDirection(): number {
    return this.direction;
  }

  public move(speed: number): void {
    if (!this.dead) {
      const dX: number = speed * Math.cos(this.direction * this.toRadians);
      const dY: number = speed * Math.sin(this.direction * this.toRadians);
      if (!this.checkHit(this.position.x + dX, this.position.y + dY)) {
        this.position.x += dX;
        this.position.y += dY;
        this.emit(BulletEvents.MOVE);
        this.dead =
          this.calculateDist(this.position, this.origin) >= this.distance;
      }
    }
  }

  // todo: this is code duplication, move to utils class
  private calculateDist(
    point1: { x: number; y: number },
    point2: { x: number; y: number }
  ): number {
    const xLen = point2.x - point1.x;
    const yLen = point2.y - point1.y;
    const xPow = Math.pow(xLen, 2);
    const yPow = Math.pow(yLen, 2);
    const dist = Math.sqrt(xPow + yPow);
    return dist;
  }

  // refactor to "visitor" pattern
  private checkHit(newX: number, newY: number): boolean {
    if (!this.obstacles) {
      return false;
    }
    const bulletBound = 0; // should be set from view boundaries
    let obst: ITile;
    let obstIndex: number;
    const isHit = this.obstacles.some((tile: ITile, i: number) => {
      // check by wall inner radius
      const halfSize = tile.size / 2;
      const dist = this.calculateDist(
        { x: newX, y: newY },
        { x: tile.x + halfSize, y: tile.y + halfSize }
      );
      obst = tile;
      obstIndex = i;
      return dist <= tile.size + bulletBound;
    });
    if (isHit) {
      this.dead = isHit;
      this.hitHandler(obst, obstIndex);
    }

    return isHit;
  }

  private hitHandler(obst: ITile, index: number): void {
    if (obst.hp && obst.hp > 0) {
      obst.hp -= this.damage;

      if (obst.view && obst.hp <= 0) {
        obst.view.parent.removeChild(obst.view);
        this.obstacles.splice(index, 1);
        const localFacade: Facade = Facade.getSpace("TanksGame");
        localFacade.getSingleton(LevelModel).removeHey(obst); // refactor, this is very bad :(
      }
    }
  }
}
