import { BaseView, Facade, Image } from "../../../../frameworck";
import { UIFactory } from "../../../../modules/ui";

export class BulletView extends BaseView {
  protected uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);

  protected onAdded() {
    super.onAdded();

    const img: Image = new Image(
      this.uiFactory.createTexture({ color: 0x000000, width: 6, height: 6 })
    );
    img.anchor.set(0.5, 0.5);
    this.addChild(img);
  }
}
