export const enum BulletEvents {
  MOVE = "BulletEvents.MOVE",
  HIT = "BulletEvents.HIT",
}
