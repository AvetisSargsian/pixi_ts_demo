import { BaseController, Facade } from "../../../frameworck";
import { BulletModel } from "../bullet/model/BulletModel";
import { LevelModel } from "../model/LevelModel";
import { TankModel } from "../tank/model/TankModel";

export class LevelManager extends BaseController {
  private localFacade: Facade = Facade.getSpace("TanksGame");
  private model: LevelModel = this.localFacade.getSingleton(LevelModel);

  public init() {
    this.model.init();
    this.startTick();
  }

  public switchActiveTank(): void {
    const tanks = this.model.getTanks();
    let index = tanks.findIndex((value: TankModel) => {
      return value.isActive();
    });
    tanks[index].setActivate(false);
    if (index >= tanks.length - 1) {
      index = 0;
    } else {
      index++;
    }
    tanks[index].setActivate(true);
  }

  protected tick(dt: number): void {
    const bullets: BulletModel[] = this.model.getBullets();

    for (let i: number = bullets.length - 1; i >= 0; i--) {
      const bullet: BulletModel = bullets[i];
      if (bullet.isDead()) {
        bullets.splice(i, 1);
        bullet.destroy();
      } else {
        bullet.move(bullet.speed * dt);
      }
    }
  }
}
