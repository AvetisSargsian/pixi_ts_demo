import { Model } from "../../../frameworck";
import { BulletModel } from "../bullet/model/BulletModel";
import { ITile } from "../field/tile/ITile";
import { TankModel } from "../tank/model/TankModel";

export class LevelModel extends Model {
  private tanks: TankModel[] = [];
  private bullets: BulletModel[] = [];
  private walls: ITile[];
  private hays: ITile[];

  public init(): void {
    this.tanks = [];
    this.bullets = [];
  }

  public setField(walls: ITile[], hays: ITile[]) {
    this.walls = walls;
    this.hays = hays;
  }

  public dispose(): void {
    this.tanks = null;
    this.bullets = null;
    super.dispose();
  }

  public getTanks(): TankModel[] {
    return this.tanks;
  }

  public getWalls(): ITile[] {
    return this.walls;
  }

  public getHays(): ITile[] {
    return this.hays;
  }

  public getBullets(): BulletModel[] {
    return this.bullets;
  }

  public addTank(tank: TankModel): void {
    tank.setActivate(this.tanks.length === 0);
    tank.setObstacles(this.walls.concat(this.hays));
    this.tanks.push(tank);
  }

  public addBullet(bullet: BulletModel): void {
    bullet.setObstacles(this.walls.concat(this.hays));
    this.bullets.push(bullet);
  }

  public removeHey(tile: ITile): void {
    if (this.hays) {
      const index = this.hays.indexOf(tile);
      if (index > -1) {
        this.hays.splice(index, 1);

        this.bullets.forEach((bullet) => {
          bullet.setObstacles(this.walls.concat(this.hays));
        });

        this.tanks.forEach((tank) => {
          tank.setObstacles(this.walls.concat(this.hays));
        });
      }
    }
  }
}
