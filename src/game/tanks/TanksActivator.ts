import {
  Facade,
  IModuleActivator,
  KeyboardEventManager,
} from "../../frameworck";
import { BulletMediator } from "./bullet/mediator/BulletMediator";
import { BulletModel } from "./bullet/model/BulletModel";
import { BulletView } from "./bullet/view/BulletView";
import { LevelManager } from "./controllers/LevelManager";
import { FieldModel } from "./field/model/FieldModel";
import { TanksSceneMediator } from "./mediators/TanksSceneMediator";
import { LevelModel } from "./model/LevelModel";
import { DoubleShotCannon } from "./tank/cannon/DoubleShotCannon";
import { SingleShotCannon } from "./tank/cannon/SingleShotCannon";
import { TripleShotCannon } from "./tank/cannon/TripleShotCannon";
import { TankInputHandler } from "./tank/controllers/TankInputHandler";
import { CannonFactory } from "./tank/factory/CannonFactory";
import { TankFactory } from "./tank/factory/TankFactory";
import { TankMediator } from "./tank/mediator/TankMediator";
import { TankModel } from "./tank/model/TankModel";
import { BlueTankView } from "./tank/view/BlueTankView";
import { GreenTankView } from "./tank/view/GreenTankView";
import { RedTankView } from "./tank/view/RedTankView";
import { TanksSceneView } from "./views/TanksSceneView";

export class TanksActivator implements IModuleActivator {
  public activate(data?: any): void {
    const localFacade: Facade = Facade.registerNameSpace("TanksGame");

    localFacade.registerClassAlias(KeyboardEventManager, KeyboardEventManager);
    // bullet
    localFacade.registerClassAlias(BulletModel, BulletModel);
    localFacade.registerClassAlias(BulletView, BulletView);
    localFacade.registerClassAlias(BulletMediator, BulletMediator);

    // tank
    localFacade.registerClassAlias(CannonFactory, CannonFactory);
    localFacade.registerClassAlias(SingleShotCannon, SingleShotCannon);
    localFacade.registerClassAlias(DoubleShotCannon, DoubleShotCannon);
    localFacade.registerClassAlias(TripleShotCannon, TripleShotCannon);
    localFacade.registerClassAlias(TankFactory, TankFactory);
    localFacade.registerClassAlias(TankModel, TankModel);
    localFacade.registerClassAlias(GreenTankView, GreenTankView);
    localFacade.registerClassAlias(RedTankView, RedTankView);
    localFacade.registerClassAlias(BlueTankView, BlueTankView);
    localFacade.registerClassAlias(TankMediator, TankMediator);
    localFacade.registerClassAlias(TankInputHandler, TankInputHandler);

    // level
    localFacade.registerSingleton(FieldModel);
    localFacade.registerSingleton(LevelModel);
    localFacade.registerSingleton(LevelManager);
    localFacade.registerClassAlias(TanksSceneView, TanksSceneView);
    localFacade.registerClassAlias(TanksSceneMediator, TanksSceneMediator);
  }
}
