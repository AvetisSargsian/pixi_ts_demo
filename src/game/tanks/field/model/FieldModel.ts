import { Model } from "../../../../frameworck";

export class FieldModel extends Model {
  public readonly fieldSide: number = 50;
  public readonly tileSize: number = 35;
  public readonly wallsCount = 50;
  public readonly haysCount = 25;
}
