import { Point } from "pixi.js";
import { BaseView, Facade, Image } from "../../../../frameworck";
import { UIFactory } from "../../../../modules/ui";
import { FieldModel } from "../model/FieldModel";
import { ITile } from "../tile/ITile";

export class FieldBuilder {
  private localFacade: Facade = Facade.getSpace("TanksGame");
  private fieldModel: FieldModel = this.localFacade.getSingleton(FieldModel);
  private uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);
  private canvas: BaseView;
  private startPos: Point;
  private walls: ITile[];
  private hays: ITile[];
  private reserved: number[];
  private fieldSide: number;
  private tileSize: number;
  private wallsCount;
  private haysCount;

  constructor() {
    this.fieldSide = this.fieldModel.fieldSide;
    this.tileSize = this.fieldModel.tileSize;
    this.wallsCount = this.fieldModel.wallsCount;
    this.haysCount = this.fieldModel.haysCount;
  }

  public getWalls(): ITile[] {
    return this.walls;
  }

  public getHays(): ITile[] {
    return this.hays;
  }

  public init(canvas: BaseView, startPos: Point): void {
    this.canvas = canvas;
    this.startPos = startPos;
    this.reserved = [];
    this.walls = [];
    this.hays = [];
  }

  public createTerrine(): void {
    for (let i = 0; i < this.fieldSide; i++) {
      for (let j = 0; j < this.fieldSide; j++) {
        const img: Image = new Image(
          this.uiFactory.createTexture({
            color: 0x255400,
            width: this.tileSize,
            height: this.tileSize,
          })
        );
        img.x = this.startPos.x + j * this.tileSize;
        img.y = this.startPos.y + i * this.tileSize;
        this.canvas.addChild(img);
      }
    }
  }

  // refactor - extract common code
  public createWalls(): void {
    const places: number[] = [];
    const area: number = this.fieldSide * this.fieldSide;
    while (places.length < this.wallsCount) {
      const place = Math.floor(Math.random() * area);
      if (places.indexOf(place) < 0) {
        places.push(place);
      }
    }
    this.reserved = this.reserved.concat(places);

    places.forEach((value: number) => {
      const x: number =
        this.startPos.x + Math.floor(value / this.fieldSide) * this.tileSize;
      const y: number =
        this.startPos.y + Math.floor(value % this.fieldSide) * this.tileSize;
      const img: Image = new Image(
        this.uiFactory.createTexture({
          color: 0x583114,
          width: this.tileSize,
          height: this.tileSize,
        })
      );
      img.x = x;
      img.y = y;
      this.canvas.addChild(img);
      this.walls.push({
        x,
        y,
        view: img,
        size: this.tileSize,
        position: value,
      });
    });
  }

  // refactor - extract common code
  public createHays(): void {
    const places: number[] = [];
    const area: number = this.fieldSide * this.fieldSide;
    while (places.length < this.haysCount) {
      const place = Math.floor(Math.random() * area);
      if (this.reserved.indexOf(place) < 0 && places.indexOf(place) < 0) {
        places.push(place);
      }
    }

    this.reserved = this.reserved.concat(places);

    places.forEach((value: number) => {
      const x: number =
        this.startPos.x + Math.floor(value / this.fieldSide) * this.tileSize;
      const y: number =
        this.startPos.y + Math.floor(value % this.fieldSide) * this.tileSize;
      const img: Image = new Image(
        this.uiFactory.createTexture({
          color: 0xe9d405,
          width: this.tileSize,
          height: this.tileSize,
        })
      );
      img.x = x;
      img.y = y;
      this.canvas.addChild(img);
      this.hays.push({
        x,
        y,
        view: img,
        size: this.tileSize,
        hp: 100,
        position: value,
      });
    });
  }
}
