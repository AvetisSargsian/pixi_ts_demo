import { Image } from "../../../../frameworck";

export interface ITile {
  x: number;
  y: number;
  size: number;
  position: number;
  hp?: number;
  view?: Image;
}
