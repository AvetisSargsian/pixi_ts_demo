import { Point } from "pixi.js";
import { TanksInternalEvents, TanksSceneView } from "../.";
import {
  BaseView,
  Facade,
  KeyboardEventManager,
  NavigationIntentsApi,
  SceneMediator,
} from "../../../frameworck";
import { LevelManager } from "../controllers/LevelManager";
import { FieldBuilder } from "../field/builder/FieldBuilder";
import { LevelModel } from "../model/LevelModel";
import { TankTypes } from "../tank/constants/TankTypes";
import { TankFactory } from "../tank/factory/TankFactory";

export class TanksSceneMediator extends SceneMediator {
  private localFacade: Facade = Facade.getSpace("TanksGame");
  private levelModel: LevelModel = this.localFacade.getSingleton(LevelModel);
  private levelManager: LevelManager =
    this.localFacade.getSingleton(LevelManager);
  private tankFactory: TankFactory =
    this.localFacade.getClassInstance(TankFactory);
  private t_keyManager: KeyboardEventManager =
    this.localFacade.getClassInstance(KeyboardEventManager, "t");

  constructor() {
    super();
    this.levelManager.init();
  }

  public dispose(): void {
    this.nativeVIew.on(
      TanksInternalEvents.GO_BACK_BUTTON_PRESSED,
      this.onButtonPressed,
      this
    );
    this.levelModel.dispose();
    this.levelManager.dispose();
    this.t_keyManager.dispose();
    super.dispose();
  }

  protected onAddedToStage(): void {
    this.nativeVIew.on(
      TanksInternalEvents.GO_BACK_BUTTON_PRESSED,
      this.onButtonPressed,
      this
    );

    const fieldBuilder: FieldBuilder = new FieldBuilder();
    fieldBuilder.init(this.nativeVIew, new Point(400, 10));
    fieldBuilder.createTerrine();
    fieldBuilder.createWalls();
    fieldBuilder.createHays();

    // I don't like this line
    this.levelModel.setField(
      fieldBuilder.getWalls().concat(),
      fieldBuilder.getHays().concat()
    );

    this.tankFactory.produceTank(
      TankTypes.RED,
      this.nativeVIew,
      new Point(300, 300)
    );
    this.tankFactory.produceTank(
      TankTypes.GREEN,
      this.nativeVIew,
      new Point(300, 400)
    );
    this.tankFactory.produceTank(
      TankTypes.BLUE,
      this.nativeVIew,
      new Point(300, 500)
    );

    this.t_keyManager.onKeyDown = (event: KeyboardEvent) => {
      this.levelManager.switchActiveTank();
    };
  }

  protected setNativeVIew(): BaseView {
    return Facade.getSpace("TanksGame").getClassInstance(
      TanksSceneView
    ) as TanksSceneView;
  }

  protected onButtonPressed(btnId: number): void {
    NavigationIntentsApi.EMIT_CHANGE_SCENE_TO(btnId);
  }
}
