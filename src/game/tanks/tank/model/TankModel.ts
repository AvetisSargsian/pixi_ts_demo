import { Point, Rectangle } from "pixi.js";
import { Application, Facade, Model } from "../../../../frameworck";
import { ITile } from "../../field/tile/ITile";
import { Cannon } from "../cannon/Cannon";
import { TankTypes } from "../constants/TankTypes";
import { TankEvents } from "../events/TankEvents";
import { CannonFactory } from "../factory/CannonFactory";

export class TankModel extends Model {
  public readonly speed: number = 2;
  private localFacade: Facade = Facade.getSpace("TanksGame");
  private cannonFactory: CannonFactory =
    this.localFacade.getClassInstance(CannonFactory);
  private cannon: Cannon;
  private active: boolean = true;
  private angle: number = 0;
  private obstacles: ITile[];

  constructor(private type: TankTypes, private position?: Point) {
    super();
    this.initCannon();
  }

  public setObstacles(walls: ITile[]): void {
    this.obstacles = walls;
  }

  public setActivate(value: boolean): void {
    this.active = value;
  }

  public isActive(): boolean {
    return this.active;
  }

  public getAngle(): number {
    return this.angle;
  }

  public moveUp(speed: number): void {
    this.setAngle(0);
    this.move(this.position.x, this.position.y - speed);
  }

  public moveDown(speed: number): void {
    this.setAngle(180);
    this.move(this.position.x, this.position.y + speed);
  }

  public moveLeft(speed: number): void {
    this.setAngle(-90);
    this.move(this.position.x - speed, this.position.y);
  }

  public moveRight(speed: number): void {
    this.setAngle(90);
    this.move(this.position.x + speed, this.position.y);
  }

  public fire(): void {
    if (!this.active) {
      return;
    }
    this.cannon.fire(this.position.clone(), this.angle - 90);
  }

  public getPosition(): Point {
    return this.position;
  }

  public setPosition(value: Point): void {
    this.position = value;
  }

  public initCannon(): void {
    this.cannon = this.cannonFactory.produce(this.type);
  }

  protected setAngle(value: number): void {
    if (!this.active) {
      return;
    }
    this.angle = value;
  }

  protected move(x: number, y: number): void {
    if (!this.active) {
      return;
    }
    if (x < 0 || x > Application.GAME_WIDTH) {
      return;
    }
    if (y < 0 || y > Application.GAME_HEIGHT) {
      return;
    }
    if (!this.checkObstacles(x, y)) {
      this.position.x = x;
      this.position.y = y;
      this.emit(TankEvents.MOVE);
    }
  }

  // this check shoul be done through "visitor" pattern
  protected checkObstacles(newX: number, newY: number): boolean {
    if (!this.obstacles || this.obstacles.length === 0) {
      return false;
    }
    const tankBound = 15; // should be set from view boundaries
    return this.obstacles.some((wall: ITile) => {
      // check by wall inner radius
      const halfSize = wall.size / 2;
      const dist = this.calculateDist(
        { x: newX, y: newY },
        { x: wall.x + halfSize, y: wall.y + halfSize }
      );
      return dist <= wall.size + tankBound;
    });
  }

  // todo: this is code duplication, move to utils class
  protected calculateDist(
    point1: { x: number; y: number },
    point2: { x: number; y: number }
  ): number {
    const xLen = point2.x - point1.x;
    const yLen = point2.y - point1.y;
    const xPow = Math.pow(xLen, 2);
    const yPow = Math.pow(yLen, 2);
    const dist = Math.sqrt(xPow + yPow);
    return dist;
  }
}
