import {
  BaseController,
  Facade,
  KeyboardEventManager,
} from "../../../../frameworck";
import { TankModel } from "../model/TankModel";

export class TankInputHandler extends BaseController {
  private localFacade: Facade = Facade.getSpace("TanksGame");
  private upKeyManager: KeyboardEventManager =
    this.localFacade.getClassInstance(KeyboardEventManager, "ArrowUp");
  private downKeyManager: KeyboardEventManager =
    this.localFacade.getClassInstance(KeyboardEventManager, "ArrowDown");
  private leftKeyManager: KeyboardEventManager =
    this.localFacade.getClassInstance(KeyboardEventManager, "ArrowLeft");
  private rightKeyManager: KeyboardEventManager =
    this.localFacade.getClassInstance(KeyboardEventManager, "ArrowRight");
  private spaceKeyManager: KeyboardEventManager =
    this.localFacade.getClassInstance(KeyboardEventManager, " ");

  private isDown: boolean = false;
  private onTikAction: (speed: number) => void = null;

  constructor(private model: TankModel) {
    super();

    const onKeyUp = () => {
      this.isDown = false;
      this.onTikAction = null;
      this.stopTick();
    };

    const onKeyDown = (action: (speed: number) => void) => {
      this.onTikAction = action.bind(this.model);
      this.isDown = true;
      this.startTick();
    };

    this.upKeyManager.onKeyUp = onKeyUp;
    this.upKeyManager.onKeyDown = () => onKeyDown(this.model.moveUp);

    this.downKeyManager.onKeyUp = onKeyUp;
    this.downKeyManager.onKeyDown = () => onKeyDown(this.model.moveDown);

    this.leftKeyManager.onKeyUp = onKeyUp;
    this.leftKeyManager.onKeyDown = () => onKeyDown(this.model.moveLeft);

    this.rightKeyManager.onKeyUp = onKeyUp;
    this.rightKeyManager.onKeyDown = () => onKeyDown(this.model.moveRight);

    this.spaceKeyManager.onKeyDown = () => this.model.fire();
  }

  public dispose(): void {
    this.upKeyManager.dispose();
    this.downKeyManager.dispose();
    this.leftKeyManager.dispose();
    this.rightKeyManager.dispose();
    this.spaceKeyManager.dispose();
    super.dispose();
  }

  protected tick(dt: number): void {
    if (this.isDown && this.onTikAction) {
      this.onTikAction(this.model.speed * dt);
    }
  }
}
