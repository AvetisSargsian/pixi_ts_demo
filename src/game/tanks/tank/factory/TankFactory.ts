import { Point } from "pixi.js";
import { BaseView, Facade } from "../../../../frameworck";
import { LevelModel } from "../../model/LevelModel";
import { TankTypes } from "../constants/TankTypes";
import { TankMediator } from "../mediator/TankMediator";
import { TankModel } from "../model/TankModel";
import { BlueTankView } from "../view/BlueTankView";
import { GreenTankView } from "../view/GreenTankView";
import { RedTankView } from "../view/RedTankView";
import { TankView } from "../view/TankView";

export class TankFactory {
  private localFacade: Facade = Facade.getSpace("TanksGame");

  public produceTank(
    type: TankTypes,
    container: BaseView,
    startPosition: Point
  ): void {
    const tankView = this.initView(type);
    this.setTankPos(tankView, startPosition);
    const tankModel = this.initModel(type, tankView);
    const tankMed: TankMediator = this.initTank(tankView, tankModel);
    tankMed.addToParent(container);
  }

  private setTankPos(tank: BaseView, position: Point): void {
    tank.x = position.x;
    tank.y = position.y;
  }

  private initView(type: TankTypes): TankView {
    switch (type) {
      case TankTypes.GREEN:
        return this.localFacade.getClassInstance(GreenTankView);
      case TankTypes.BLUE:
        return this.localFacade.getClassInstance(BlueTankView);
      case TankTypes.RED:
        return this.localFacade.getClassInstance(RedTankView);
    }
  }

  private initModel(type: TankTypes, view: TankView): TankModel {
    const levelModel: LevelModel = this.localFacade.getSingleton(LevelModel);
    const tank: TankModel = this.localFacade.getClassInstance(
      TankModel,
      type,
      view.position
    );
    levelModel.addTank(tank);
    return tank;
  }

  private initTank(view: TankView, model: TankModel): TankMediator {
    return this.localFacade.getClassInstance(TankMediator, model, view);
  }
}
