import { Facade } from "../../../../frameworck";
import { Cannon } from "../cannon/Cannon";
import { DoubleShotCannon } from "../cannon/DoubleShotCannon";
import { SingleShotCannon } from "../cannon/SingleShotCannon";
import { TripleShotCannon } from "../cannon/TripleShotCannon";
import { TankTypes } from "../constants/TankTypes";

export class CannonFactory {
  public produce(type: TankTypes): Cannon {
    const localFacade: Facade = Facade.getSpace("TanksGame");
    switch (type) {
      case TankTypes.GREEN:
        return localFacade.getClassInstance(SingleShotCannon);
      case TankTypes.RED:
        return localFacade.getClassInstance(DoubleShotCannon);
      case TankTypes.BLUE:
        return localFacade.getClassInstance(TripleShotCannon);
      default:
        return null;
    }
  }
}
