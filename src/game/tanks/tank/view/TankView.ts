import { Application, BaseView, Facade, Image } from "../../../../frameworck";
import { UIFactory } from "../../../../modules/ui";

export class TankView extends BaseView {
  protected uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);

  protected drawBody(): void {
    const body = new Image(
      this.uiFactory.createTexture({ color: 0xffffff, width: 30, height: 35 })
    );
    body.anchor.set(0.5, 0.5);
    this.addChild(body);
  }

  protected onAdded() {
    super.onAdded();

    this.drawBody();

    const caterpillarTexture = this.uiFactory.createTexture({
      color: 0x000000,
      width: 7,
      height: 39,
    });
    const caterpillarLeft: Image = new Image(caterpillarTexture);
    const caterpillarRight: Image = new Image(caterpillarTexture);
    caterpillarLeft.x = -7;
    caterpillarLeft.y = (35 - 39) / 2;
    caterpillarRight.x = 30;
    caterpillarRight.y = (35 - 39) / 2;
    this.addChild(caterpillarLeft);
    this.addChild(caterpillarRight);

    const cannon: Image = new Image(
      this.uiFactory.createTexture({ color: 0x000000, width: 6, height: 30 })
    );
    cannon.y = -15;
    cannon.x = 30 / 2 - 6 / 2;
    this.addChild(cannon);

    const tower: Image = new Image(
      this.uiFactory.createTexture({ color: 0x000000, width: 14, height: 14 })
    );
    tower.y = 35 / 2 - 14 / 2;
    tower.x = 30 / 2 - 14 / 2;
    this.addChild(tower);

    this.pivot.set(30 / 2, 35 / 2);
  }
}
