import { Image } from "../../../../frameworck";
import { TankView } from "./TankView";

export class GreenTankView extends TankView {
  protected drawBody(): void {
    this.addChild(
      new Image(
        this.uiFactory.createTexture({ color: 0x00ff00, width: 30, height: 35 })
      )
    );
  }
}
