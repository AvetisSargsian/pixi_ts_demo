import { Image } from "../../../../frameworck";
import { TankView } from "./TankView";

export class BlueTankView extends TankView {
  protected drawBody(): void {
    this.addChild(
      new Image(
        this.uiFactory.createTexture({ color: 0x0000ff, width: 30, height: 35 })
      )
    );
  }
}
