import { Image } from "../../../../frameworck";
import { TankView } from "./TankView";

export class RedTankView extends TankView {
  protected drawBody(): void {
    this.addChild(
      new Image(
        this.uiFactory.createTexture({ color: 0xff0000, width: 30, height: 35 })
      )
    );
  }
}
