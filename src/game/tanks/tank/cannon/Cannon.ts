import { Point } from "pixi.js";

export abstract class Cannon {
  public fire(position: Point, angle: number) {
    this.createBullet(position, angle);
  }

  protected abstract createBullet(position: Point, angle: number): void;
}
