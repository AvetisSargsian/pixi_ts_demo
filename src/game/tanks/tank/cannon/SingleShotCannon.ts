import { Point } from "pixi.js";
import { Facade, SceneMediator } from "../../../../frameworck";
import { NavigationModel } from "../../../../modules/navigation";
import { BulletMediator } from "../../bullet/mediator/BulletMediator";
import { BulletModel } from "../../bullet/model/BulletModel";
import { TanksSceneMediator } from "../../mediators/TanksSceneMediator";
import { LevelModel } from "../../model/LevelModel";
import { Cannon } from "./Cannon";

export class SingleShotCannon extends Cannon {
  protected createBullet(position: Point, angle: number): void {
    const localFacade: Facade = Facade.getSpace("TanksGame");
    const parent: SceneMediator =
      Facade.global.getSingleton(NavigationModel).currentSceneMediator;
    const bulletModel: BulletModel = localFacade.getClassInstance(
      BulletModel,
      position,
      angle,
      25
    );
    const mediator: BulletMediator = localFacade.getClassInstance(
      BulletMediator,
      bulletModel
    );
    mediator.addToParent(parent.getNativeVIew());
    const levelModel: LevelModel = localFacade.getSingleton(LevelModel);
    levelModel.addBullet(bulletModel);
  }
}
