import { Point } from "pixi.js";
import { Facade, SceneMediator } from "../../../../frameworck";
import { NavigationModel } from "../../../../modules/navigation";
import { BulletMediator } from "../../bullet/mediator/BulletMediator";
import { BulletModel } from "../../bullet/model/BulletModel";
import { LevelModel } from "../../model/LevelModel";
import { Cannon } from "./Cannon";

export class TripleShotCannon extends Cannon {
  protected createBullet(position: Point, angle: number): void {
    // tepm solution
    const localFacade: Facade = Facade.getSpace("TanksGame");
    const parent: SceneMediator =
      Facade.global.getSingleton(NavigationModel).currentSceneMediator;
    for (let index = 0; index < 3; index++) {
      const pos = position.clone();
      pos.x += index * 5;
      pos.y += index * 5;
      const bulletModel: BulletModel = localFacade.getClassInstance(
        BulletModel,
        pos,
        angle,
        20
      );
      const mediator: BulletMediator = localFacade.getClassInstance(
        BulletMediator,
        bulletModel
      );
      mediator.addToParent(parent.getNativeVIew());
      const levelModel: LevelModel = localFacade.getSingleton(LevelModel);
      levelModel.addBullet(bulletModel);
    }
  }
}
