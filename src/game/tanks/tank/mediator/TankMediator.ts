import { ComponentMediator, Facade } from "../../../../frameworck";
import { TankInputHandler } from "../controllers/TankInputHandler";
import { TankEvents } from "../events/TankEvents";
import { TankModel } from "../model/TankModel";
import { TankView } from "../view/TankView";

export class TankMediator extends ComponentMediator {
  private inputHandler: TankInputHandler;

  constructor(private model: TankModel, view?: TankView) {
    super(view);
  }

  public dispose(): void {
    this.model.dispose();
    this.inputHandler.dispose();
    super.dispose();
  }

  protected onAddedToStage(): void {
    const localFacade: Facade = Facade.getSpace("TanksGame");
    this.inputHandler = localFacade.getClassInstance(
      TankInputHandler,
      this.model
    );
    this.model.on(TankEvents.MOVE, this.onTankMove, this);
  }

  private onTankMove(): void {
    const pos = this.model.getPosition();
    this.nativeVIew.angle = this.model.getAngle();
    // this.nativeVIew.x = pos.x;
    // this.nativeVIew.y = pos.y;
  }
}
