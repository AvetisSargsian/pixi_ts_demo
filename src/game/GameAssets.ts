import { IAssets } from "../frameworck";

export const GameAssets: IAssets = {
  load: {
    cards: "assets/atlas.json",
    snake: "assets/snake.png",
    star: "assets/star_small.png",
    blue_stone: "assets/blue_stone.png",
    fire_plate: "assets/fire_plate.png",
    ice_plate: "assets/ice_plate.png",
    violet_stone: "assets/violet_stone.png",
    progress_bar: "assets/progress_bar.png",
    orange_panel: "assets/arrow_orange.png",
    gray_panel: "assets/button_gray.png",
    // ambient: "assets/ambient/ambient.skel",
  },
};
