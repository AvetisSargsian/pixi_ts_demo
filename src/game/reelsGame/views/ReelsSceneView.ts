import { ReelsInternalEvents } from "..";
import {
  Application,
  BaseView,
  Facade,
  TextField,
  TextStyle,
} from "../../../frameworck";
import {
  Button,
  ButtonProperties,
  Switch,
  SwitchProperties,
  UIFactory,
} from "../../../modules/ui";
import { ScenesList } from "../../navigation/ScenesList";

export class ReelsSceneView extends BaseView {
  public static SPIN_BUTTON_ID: number = 303030;
  public static AUTO_SPIN_BUTTON_ID: number;
  public static TURBO_BUTTON_ID: number;
  public static GO_BACK_BUTTON_ID: number;
  private uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);

  protected onAdded() {
    super.onAdded();
    const backBtn: Button = this.uiFactory.createUiButton({
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      text: "Main Menu",
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () =>
        this.emit(ReelsInternalEvents.GO_BACK_BUTTON_PRESSED, ScenesList.MENU),
    } as ButtonProperties);
    ReelsSceneView.GO_BACK_BUTTON_ID = backBtn.id;
    backBtn.y = Application.GAME_HEIGHT - backBtn.height;
    this.addChild(backBtn);

    const autoSpinSwitch: Switch = this.uiFactory.createUiSwitch({
      upState: this.uiFactory.createTexture({
        color: 0xffcc34,
        width: 60,
        height: 40,
      }),
      text: "AS",
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 60,
        height: 40,
      }),
      overState: this.uiFactory.createTexture({
        color: 0x119977,
        width: 60,
        height: 40,
      }),
      callBack: () =>
        this.emit(
          ReelsInternalEvents.AUTO_SPIN_BUTTON_PRESSED,
          ReelsSceneView.AUTO_SPIN_BUTTON_ID
        ),
    } as SwitchProperties);
    ReelsSceneView.AUTO_SPIN_BUTTON_ID = autoSpinSwitch.id;
    autoSpinSwitch.y = Application.GAME_HEIGHT - backBtn.height;
    autoSpinSwitch.x = 250;
    this.addChild(autoSpinSwitch);

    const turboSwitch: Switch = this.uiFactory.createUiSwitch({
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 70,
        height: 40,
      }),
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 70,
        height: 40,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 70,
        height: 40,
      }),
      text: "TURBO",
      callBack: () =>
        this.emit(
          ReelsInternalEvents.TURBO_BUTTON_PRESSED,
          ReelsSceneView.TURBO_BUTTON_ID
        ),
    } as SwitchProperties);

    ReelsSceneView.TURBO_BUTTON_ID = turboSwitch.id;
    turboSwitch.y = Application.GAME_HEIGHT - backBtn.height;
    turboSwitch.x = 320;
    this.addChild(turboSwitch);

    const style = new TextStyle({
      fontFamily: "Arial",
      fontSize: 36,
      fontStyle: "italic",
      fontWeight: "bold",
      fill: ["#ffffff", "#00ff99"], // gradient
      stroke: "#4a1850",
      strokeThickness: 5,
      dropShadow: true,
      dropShadowColor: "#000000",
      dropShadowBlur: 4,
      dropShadowAngle: Math.PI / 6,
      dropShadowDistance: 6,
      wordWrap: true,
      wordWrapWidth: 440,
    });

    // todo:: create UIElement TextField
    const playText = new TextField("Push ME!", style);
    this.addChild(playText);
    playText.interactive = true;
    playText.buttonMode = true;
    playText.position.y = 100;
    playText.position.x = 100;
    playText.addListener("pointerdown", () =>
      this.emit(
        ReelsInternalEvents.SPIN_BUTTON_PRESSED,
        ReelsSceneView.SPIN_BUTTON_ID
      )
    );
  }
}
