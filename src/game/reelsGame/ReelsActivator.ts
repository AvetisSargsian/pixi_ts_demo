import { Facade, IModuleActivator } from "../../frameworck";
import { AutoSpinPressedAction } from "./components/reel/actions/AutoSpinPressedAction";
import { SpinPressedAction } from "./components/reel/actions/SpinPressedAction";
import { TurboPressedAction } from "./components/reel/actions/TurboPressedAction";
import { ReelComponentConfig } from "./components/reel/config/ReelComponentConfig";
import { ReelsViewMediator } from "./components/reel/mediator/ReelsMediator";
import { ReelsModel } from "./components/reel/models/ReelsModel";
import { ReelsView } from "./components/reel/view/ReelsView";
import { ReelsInternalEvents } from "./events/ReelsInternalEvents";
import { ReelsSceneMediator } from "./mediators/ReelsSceneMediator";
import { ReelsSceneView } from "./views/ReelsSceneView";

export class ReelsActivator implements IModuleActivator {
  public activate(data?: any): void {
    const localFacade: Facade = Facade.registerNameSpace("ReelsGame");

    // scene
    localFacade.registerClassAlias(ReelsSceneView);
    localFacade.registerClassAlias(ReelsSceneMediator);

    // component
    localFacade.registerSingleton(ReelsModel);
    localFacade.registerSingleton(ReelComponentConfig);

    localFacade.registerClassAlias(ReelsView);
    localFacade.registerClassAlias(ReelsViewMediator);

    localFacade.bindCommandToEvent(
      ReelsInternalEvents.AUTO_SPIN_BUTTON_PRESSED,
      AutoSpinPressedAction
    );
    localFacade.bindCommandToEvent(
      ReelsInternalEvents.TURBO_BUTTON_PRESSED,
      TurboPressedAction
    );
    localFacade.bindCommandToEvent(
      ReelsInternalEvents.SPIN_BUTTON_PRESSED,
      SpinPressedAction
    );
  }
}
