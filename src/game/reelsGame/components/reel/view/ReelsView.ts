import {
  Application,
  BaseView,
  Facade,
  Graphics,
  Image,
} from "../../../../../frameworck";
import { ReelsModel } from "../models/ReelsModel";
import { ReelVO } from "../models/ReelVO";
import { SymbolVO } from "../models/SymbolVO";

export class ReelsView extends BaseView {
  private readonly REEL_WIDTH = 82;
  private readonly SYMBOL_SIZE = 72;
  private reels: any[] = [];
  // get rid of model, not good to have model in view
  private model: ReelsModel =
    Facade.getSpace("ReelsGame").getSingleton(ReelsModel);

  public onAdded() {
    super.onAdded();

    this.model.slotReels.forEach((reel: ReelVO, reelIndex: number) => {
      const rc = new BaseView();
      rc.x = reelIndex * this.REEL_WIDTH;
      this.addChild(rc);
      const reelsCont = {
        symbolsView: [],
      };
      reel.symbols.forEach((symbol: SymbolVO, symbolIndex: number) => {
        const symbolImg: Image = new Image(
          this.resources[symbol.textureName].texture
        );
        symbolImg.y = symbolIndex * this.SYMBOL_SIZE;
        symbolImg.x = Math.round((this.SYMBOL_SIZE - symbolImg.width) / 2);
        reelsCont.symbolsView.push(symbolImg);
        rc.addChild(symbolImg);
      });
      this.reels.push(reelsCont);
    });

    const reelsMask = new Graphics();
    reelsMask.beginFill(0, 0.2);
    reelsMask.drawRect(
      0,
      this.SYMBOL_SIZE - this.SYMBOL_SIZE,
      this.REEL_WIDTH * this.model.reelsCount,
      this.SYMBOL_SIZE * this.model.reelVisibleHeight
    );
    reelsMask.endFill();

    this.mask = reelsMask;
    this.addChild(reelsMask);

    this.y =
      (Application.GAME_HEIGHT -
        this.SYMBOL_SIZE * this.model.reelVisibleHeight) /
      2;
    this.x = (Application.GAME_WIDTH - this.width) / 2;
  }

  public onReelsStartRunning(isRunning: boolean): void {
    if (isRunning) {
      Application.TICKER().add(this.animateUpdate, this);
    } else {
      Application.TICKER().remove(this.animateUpdate, this);
    }
  }

  private animateUpdate(delta: number): void {
    this.reels.forEach((r, reelIndex) => {
      // Update symbol positions on reel.
      r.symbolsView.forEach((symbol, symbolIndex: number) => {
        const prevYPos = symbol.y;
        symbol.y =
          ((this.model.slotReels[reelIndex].position + symbolIndex) %
            r.symbolsView.length) *
            this.SYMBOL_SIZE -
          this.SYMBOL_SIZE;
        if (symbol.y < 0 && prevYPos > this.SYMBOL_SIZE) {
          // Detect going over and swap a texture.
          // This should in proper product be determined from some logical reel.
          // symbol.texture = this.slotTextures[Math.floor(Math.random() * this.slotTextures.length)].texture;
          // symbol.x = Math.round((this.SYMBOL_SIZE - symbol.width) / 2);
        }
      });
    });
  }
}
