import { Back } from "gsap";
import { BaseCommand, Facade, Utils } from "../../../../../frameworck";
import { ReelsModel, ReelVO } from "../../../index";

export class SpinPressedAction extends BaseCommand {
  public execute(data?: any): void {
    const model: ReelsModel =
      Facade.getSpace("ReelsGame").getSingleton(ReelsModel);

    function reelsRotationComplete(): void {
      model.slotReels.forEach((reel: ReelVO, index: number) => {
        if (reel.position > model.symbolsInReel) {
          reel.position = reel.position % model.symbolsInReel;
          reel.target = 0;
          console.log(index + " " + reel.position);
        }
      });
      model.running = false;

      if (model.isAutoSpinEnabled) {
        startRotateReels();
      }
    }

    function startRotateReels(): void {
      if (model.running) {
        return;
      }

      model.slotReels.forEach((reelVo: ReelVO, index: number) => {
        const extra = Math.floor(Math.random() * 10);
        reelVo.target = reelVo.position + 10 + index * 5 + extra;
        console.log(index + " " + reelVo.target);
        const time: number = model.isTurboEnabled
          ? 1 + index * 0.2
          : 2.5 + index * 0.6;

        Utils.tweenTo(reelVo, time, {
          position: reelVo.target,
          ease: Back.easeOut.config(0.5),
          onComplete:
            index === model.reelsCount - 1 ? reelsRotationComplete : null,
        });
      });

      model.running = true;
    }

    startRotateReels();
  }
}
