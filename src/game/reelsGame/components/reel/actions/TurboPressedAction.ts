import { BaseCommand, Facade } from "../../../../../frameworck";
import { ReelsModel } from "../../../index";

export class TurboPressedAction extends BaseCommand {
  public execute(data?: any): void {
    const model: ReelsModel =
      Facade.getSpace("ReelsGame").getSingleton(ReelsModel);
    model.turbo = !model.isTurboEnabled;
  }
}
