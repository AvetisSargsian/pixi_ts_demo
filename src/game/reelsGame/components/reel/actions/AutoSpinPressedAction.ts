import { BaseCommand, Facade } from "../../../../../frameworck";
import { ReelsModel } from "../../../index";

export class AutoSpinPressedAction extends BaseCommand {
  public execute(data?: any): void {
    const model: ReelsModel =
      Facade.getSpace("ReelsGame").getSingleton(ReelsModel);
    model.autoSpin = !model.isAutoSpinEnabled;
  }
}
