import { SymbolVO } from "./SymbolVO";

export class ReelVO {
  public position: number = 0;
  public target: number = 0;
  public symbols: SymbolVO[] = [];
}
