import { Assets, Model } from "../../../../../frameworck";
import { ReelsInternalEvents } from "../../../events/ReelsInternalEvents";
import { ReelVO } from "./ReelVO";
import { SymbolVO } from "./SymbolVO";

export class ReelsModel extends Model {
  public readonly reelsCount: number = 5;
  public readonly symbolsInReel: number = 7;
  public readonly reelVisibleHeight: number = 3;

  public readonly slotTextures = [
    Assets.load.blue_stone,
    Assets.load.fire_plate,
    Assets.load.ice_plate,
    Assets.load.violet_stone,
  ];

  private reels: ReelVO[] = [];
  private _running: boolean;
  private _autoSpin: boolean;
  private _turbo: boolean;

  public constructor() {
    super();

    for (let i = 0, len: number = this.reelsCount; i < len; i++) {
      const reel: ReelVO = new ReelVO();
      for (let j = 0, len1: number = this.symbolsInReel; j < len1; j++) {
        const symbol: SymbolVO = new SymbolVO();
        const index = Math.floor(Math.random() * this.slotTextures.length);
        symbol.textureName = this.slotTextures[index];
        reel.symbols.push(symbol);
      }
      this.reels.push(reel);
    }
  }

  public get slotReels(): ReelVO[] {
    return this.reels;
  }

  public set running(value: boolean) {
    this._running = value;
    this.emit(ReelsInternalEvents.REELS_RUNNING, this._running);
  }

  public get running() {
    return this._running;
  }

  public get isAutoSpinEnabled(): boolean {
    return this._autoSpin;
  }

  public set autoSpin(value: boolean) {
    this._autoSpin = value;
  }

  public set turbo(value: boolean) {
    this._turbo = value;
  }

  public get isTurboEnabled(): boolean {
    return this._turbo;
  }
}
