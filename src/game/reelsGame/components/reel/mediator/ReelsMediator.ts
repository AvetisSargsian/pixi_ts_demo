import { BaseView, CompositeMediator, Facade } from "../../../../../frameworck";
import { ReelsInternalEvents } from "../../../events/ReelsInternalEvents";
import { ReelsModel } from "../models/ReelsModel";
import { ReelsView } from "../view/ReelsView";

export class ReelsViewMediator extends CompositeMediator {
  private localFacade: Facade = Facade.getSpace("ReelsGame");
  private model: ReelsModel = this.localFacade.getSingleton(ReelsModel);

  public dispose(): void {
    this.model.off(
      ReelsInternalEvents.REELS_RUNNING,
      this.onReelsRunning,
      this
    );
    this.model = null;
    super.dispose();
  }

  protected onAddedToStage(): void {
    this.model.on(ReelsInternalEvents.REELS_RUNNING, this.onReelsRunning, this);
  }

  protected setNativeVIew(): BaseView {
    return Facade.getSpace("ReelsGame").getClassInstance(ReelsView);
  }

  private onReelsRunning(isRunning: boolean): void {
    (this.nativeVIew as ReelsView).onReelsStartRunning(isRunning);
  }
}
