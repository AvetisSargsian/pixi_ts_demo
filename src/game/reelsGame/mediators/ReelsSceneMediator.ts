import {
  BaseView,
  Facade,
  NavigationIntentsApi,
  SceneMediator,
} from "../../../frameworck";
import { ReelsViewMediator } from "../components/reel/mediator/ReelsMediator";
import { ReelsInternalEvents } from "../events/ReelsInternalEvents";
import { ReelsSceneView } from "../views/ReelsSceneView";

export class ReelsSceneMediator extends SceneMediator {
  private localFacade: Facade = Facade.getSpace("ReelsGame");
  private reelsViewMediator: ReelsViewMediator;

  protected addViewListeners() {
    this.nativeVIew.on(
      ReelsInternalEvents.GO_BACK_BUTTON_PRESSED,
      this.onBackButtonPressed,
      this
    );
    this.nativeVIew.on(
      ReelsInternalEvents.AUTO_SPIN_BUTTON_PRESSED,
      this.onButtonPush,
      this
    );
    this.nativeVIew.on(
      ReelsInternalEvents.TURBO_BUTTON_PRESSED,
      this.onButtonPush,
      this
    );
    this.nativeVIew.on(
      ReelsInternalEvents.SPIN_BUTTON_PRESSED,
      this.onButtonPush,
      this
    );
  }

  protected onAddedToStage(): void {
    super.onAddedToStage();
    this.addViewListeners();

    this.reelsViewMediator =
      this.localFacade.getClassInstance(ReelsViewMediator);
    this.reelsViewMediator.addToParent(this.nativeVIew);
  }

  protected onButtonPush(uiId: number): void {
    let message: ReelsInternalEvents;
    switch (uiId) {
      case ReelsSceneView.AUTO_SPIN_BUTTON_ID:
        message = ReelsInternalEvents.AUTO_SPIN_BUTTON_PRESSED;
        break;
      case ReelsSceneView.TURBO_BUTTON_ID:
        message = ReelsInternalEvents.TURBO_BUTTON_PRESSED;
        break;
      case ReelsSceneView.SPIN_BUTTON_ID:
        message = ReelsInternalEvents.SPIN_BUTTON_PRESSED;
        break;
    }
    this.localFacade.dispatcher.emit(message);
  }

  protected setNativeVIew(): BaseView {
    return Facade.getSpace("ReelsGame").getClassInstance(
      ReelsSceneView
    ) as ReelsSceneView;
  }

  protected onBackButtonPressed(btnId: number): void {
    NavigationIntentsApi.EMIT_CHANGE_SCENE_TO(btnId);
  }
}
