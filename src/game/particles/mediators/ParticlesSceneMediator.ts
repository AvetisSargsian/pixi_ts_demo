import {
  BaseView,
  NavigationIntentsApi,
  SceneMediator,
} from "../../../frameworck";
import { ParticlesScene } from "../views/ParticlesScene";

export class ParticlesSceneMediator extends SceneMediator {
  protected setNativeVIew(): BaseView {
    return new ParticlesScene();
  }

  protected onAddedToStage(): void {
    super.onAddedToStage();

    this.nativeVIew.on(
      ParticlesScene.BUTTON_PRESSED,
      this.onButtonPressed,
      this
    );
  }

  protected onButtonPressed(btnId: number) {
    NavigationIntentsApi.EMIT_CHANGE_SCENE_TO(btnId);
  }
}
