import { Emitter } from "pixi-particles";
import { Application, Assets, BaseView, Facade } from "../../../frameworck";
import { Button, ButtonProperties, UIFactory } from "../../../modules/ui";
import { ScenesList } from "../../navigation/ScenesList";

export class ParticlesScene extends BaseView {
  public static readonly BUTTON_PRESSED: string =
    "ParticlesScene.BUTTON_PRESSED";

  private uiFactory: UIFactory = Facade.global.getSingleton(UIFactory);
  private emitter!: Emitter;

  protected update(dt: number) {
    this.emitter.update(dt * 0.001);
  }

  protected onRemoved() {
    Application.TICKER().remove(this.update.bind(this));
    super.onRemoved();
  }

  protected onAdded() {
    super.onAdded();

    const backBtn: Button = this.uiFactory.createUiButton({
      text: "Main Menu",
      upState: this.uiFactory.createTexture({
        color: 0xff6634,
        width: 200,
        height: 70,
      }),
      downState: this.uiFactory.createTexture({
        color: 0xff9977,
        width: 200,
        height: 70,
      }),
      overState: this.uiFactory.createTexture({
        color: 0xcc9977,
        width: 200,
        height: 70,
      }),
      callBack: () => this.emit(ParticlesScene.BUTTON_PRESSED, ScenesList.MENU),
    } as ButtonProperties);
    backBtn.y = Application.GAME_HEIGHT - backBtn.height;
    this.addChild(backBtn);

    const texture = this.resources[Assets.load.star].texture;

    this.emitter = new Emitter(this, [texture], {
      alpha: {
        list: [
          {
            value: 0.8,
            time: 0,
          },
          {
            value: 0.1,
            time: 1,
          },
        ],
        isStepped: false,
      },
      scale: {
        list: [
          {
            value: 1,
            time: 0,
          },
          {
            value: 0.3,
            time: 1,
          },
        ],
        isStepped: false,
      },
      color: {
        list: [
          {
            value: "fb1010",
            time: 0,
          },
          {
            value: "f5b830",
            time: 1,
          },
        ],
        isStepped: false,
      },
      speed: {
        list: [
          {
            value: 200,
            time: 0,
          },
          {
            value: 100,
            time: 1,
          },
        ],
        isStepped: false,
      },
      startRotation: {
        min: 0,
        max: 360,
      },
      rotationSpeed: {
        min: 0,
        max: 0,
      },
      lifetime: {
        min: 0.5,
        max: 1.5,
      },
      frequency: 0.008,
      spawnChance: 1,
      particlesPerWave: 1,
      emitterLifetime: 1000.0,
      maxParticles: 100,
      pos: {
        x: 600,
        y: 300,
      },
      addAtBack: false,
      spawnType: "circle",
      spawnCircle: {
        x: 0,
        y: 0,
        r: 10,
      },
    });
    this.emitter.emit = true;
    Application.TICKER().add(this.update.bind(this));
  }
}
