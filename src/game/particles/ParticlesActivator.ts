import { Facade, IModuleActivator } from "../../frameworck";
import { ParticlesSceneMediator, ParticlesScene } from ".";

export class ParticlesActivator implements IModuleActivator {
  activate(data?: any) {
    const localFacade: Facade = Facade.registerNameSpace("ParticlesGame");
    // scene
    localFacade.registerClassAlias(ParticlesScene);
    localFacade.registerClassAlias(ParticlesSceneMediator);
  }
}
