declare const __PROD__: boolean;
declare const __STAGING__: boolean;
declare const __DEV__: boolean;
declare const __MOBILE__: boolean;
declare const __DESKTOP__: boolean;
declare const __PLATFORM__: "desktop" | "web";
declare const __OS__: "MAC" | "Windows" | "Linux";
