import * as PIXI from "pixi.js";
import type { Dict } from "@pixi/utils";
import { FPSCounter, MainStage } from ".";
import { Facade } from "./mmvc/facade/Facade";
import { IModuleActivator } from "./mmvc/interfaces";
import { LoaderResource } from "pixi.js";
export class Application {
  public static readonly READY: string = "Application.READY";
  public static GAME_WIDTH: number = 1366;
  public static GAME_HEIGHT: number = 768;

  public static RESOURCES(): Dict<LoaderResource> {
    return this.resources;
  }

  public static LOADER(): PIXI.Loader {
    return this.loader;
  }

  public static RENDERER(): PIXI.Renderer {
    return this.renderer;
  }

  public static TICKER(): PIXI.Ticker {
    return this.ticker;
  }

  public static STAGE(): MainStage {
    return this.stage;
  }

  private static loader: PIXI.Loader;
  private static renderer: PIXI.Renderer;
  private static ticker: PIXI.Ticker;
  private static stage: PIXI.Container;
  private static resources: Dict<LoaderResource>;
  private static fpsCounter: FPSCounter;

  private static onTick(dt: number) {
    Application.renderer.render(Application.stage);
    if (Application.fpsCounter) {
      Application.fpsCounter.update();
    }
  }

  private static resize() {
    const windowInnerWidth: number = window.innerWidth;
    const windowInnerHeight: number = window.innerHeight;
    const scaleFactor: number = Math.min(
      windowInnerWidth / Application.GAME_WIDTH,
      windowInnerHeight / Application.GAME_HEIGHT
    );

    const newWidth: number = Math.ceil(Application.GAME_WIDTH * scaleFactor);
    const newHeight: number = Math.ceil(Application.GAME_HEIGHT * scaleFactor);

    let marginHorizontal: number = 0;
    let marginVertical: number = 0;
    if (newWidth > newHeight) {
      if (newWidth < windowInnerWidth) {
        marginHorizontal = (windowInnerWidth - newWidth) / 2;
      } else {
        marginVertical = (windowInnerHeight - newHeight) / 2;
      }
    } else {
      if (newHeight < windowInnerHeight) {
        marginHorizontal = (windowInnerWidth - newWidth) / 2;
      } else {
        marginVertical = (windowInnerHeight - newHeight) / 2;
      }
    }
    // change position of canvas and scale up if scaleFactor > 1
    Application.centerCanvas(
      marginHorizontal,
      marginVertical,
      newWidth,
      newHeight,
      scaleFactor
    );

    if (scaleFactor <= 1.1) {
      // change actual count of pixels inside canvas,
      // this will impact performance, more pixels in canvas better look but difficult to draw.
      Application.renderer.resize(newWidth, newHeight);
      Application.stage.scale.set(scaleFactor);
    }

    // Whether you really want to adjust for HD-DPI is up to you.
    // On iPhoneX or iPhone11 window.devicePixelRatio is 3 which means you'll be drawing 9 times as many pixels.
    // On A Samsung Galaxy S8 that value is 5 which means you'd be drawing 16 times as many pixels.
    // That can really slow down your program. In fact it's a common optimization in games to actually
    // render less pixels than are displayed and let the GPU scale them up.
    // It really depends on what your needs are. If you're drawing a graph for printing you might
    // want to support HD-DPI. If you're making a game you might not or you might want to give the user
    // the option to turn support on or off if their system is not fast enough to draw so many pixels.
  }

  private static centerCanvas(
    marginHorizont: number,
    marginVertical: number,
    width: number,
    height: number,
    scale: number
  ) {
    const canvas: HTMLCanvasElement = Application.renderer.view;
    canvas.style.marginTop = marginVertical + "px";
    canvas.style.marginBottom = marginVertical + "px";
    canvas.style.marginLeft = marginHorizont + "px";
    canvas.style.marginRight = marginHorizont + "px";
    canvas.style.display = "block";
    canvas.style.width = `${width}px`;
    canvas.style.height = `${height}px`;
  }

  constructor(parent: HTMLElement, width: number, height: number) {
    Application.renderer = new PIXI.Renderer({
      width,
      height,
      backgroundColor: 0x1099bb,
    });
    parent.appendChild(Application.renderer.view);

    Application.stage = new PIXI.Container();
    Application.ticker = new PIXI.Ticker();
    Application.ticker.add(Application.onTick, this);
    Application.ticker.start();

    window.onresize = Application.resize;
    Application.resize();

    Application.loader = PIXI.Loader.shared;
    Application.resources = Application.loader.resources;
  }

  public start(mainActivator: IModuleActivator): Promise<void> {
    mainActivator.activate();
    Application.fpsCounter = new FPSCounter();
    Facade.global.dispatch({ name: Application.READY });
    return Promise.resolve();
  }
}
