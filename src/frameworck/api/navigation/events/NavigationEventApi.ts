import { Facade, NavigationEvents, SceneMediator } from "../../..";

export class NavigationEventApi {
  public static EMIT_SCENE_CHANGED(scene: SceneMediator): void {
    Facade.global.dispatcher.emit(NavigationEvents.SCENE_CHANGED, scene);
  }

  public static ON_SCENE_CHANGED(
    listener: (scene: SceneMediator) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.on(
      NavigationEvents.SCENE_CHANGED,
      listener,
      context
    );
  }

  public static OFF_SCENE_CHANGED(listener: () => void, context?: any): void {
    Facade.global.dispatcher.off(
      NavigationEvents.SCENE_CHANGED,
      listener,
      context
    );
  }
}
