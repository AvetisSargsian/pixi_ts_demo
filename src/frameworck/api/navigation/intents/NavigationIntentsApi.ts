import { Facade, NavigationIntents } from "../../..";

export class NavigationIntentsApi {
  public static EMIT_CHANGE_SCENE_TO(sceneId: number | string): void {
    Facade.global.dispatcher.emit(NavigationIntents.CHANGE_SCENE_TO, sceneId);
  }

  public static ON_CHANGE_SCENE_TO(
    listener: (sceneId: number | string) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.on(
      NavigationIntents.CHANGE_SCENE_TO,
      listener,
      context
    );
  }

  public static OFF_CHANGE_SCENE_TO(
    listener: (sceneId: number | string) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.off(
      NavigationIntents.CHANGE_SCENE_TO,
      listener,
      context
    );
  }
}
