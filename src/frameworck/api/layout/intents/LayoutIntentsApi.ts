import { DisplayObject } from "pixi.js";
import { BaseView, Facade, LayoutIntents, LayoutList } from "../../..";

export class LayoutIntentsApi {
  public static EMIT_ADD_VIEW(layout: LayoutList, view: DisplayObject): void {
    Facade.global.dispatcher.emit(LayoutIntents.ADD_VIEW, layout, view);
  }

  public static ON_ADD_VIEW(
    listener: (layout: LayoutList, view: BaseView) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.on(LayoutIntents.ADD_VIEW, listener, context);
  }

  public static OFF_ADD_VIEW(
    listener: (layout: LayoutList, view: BaseView) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.off(LayoutIntents.ADD_VIEW, listener, context);
  }
}
