import { DisplayObject } from "pixi.js";
import { Facade } from "../../..";
import { LayoutEvents } from "./LayoutEvents";

export class LayoutEventApi {
  public static EMIT_VIEW_ADDED(view: DisplayObject): void {
    Facade.global.dispatcher.emit(LayoutEvents.VIEW_ADDED, view);
  }

  public static ON_VIEW_ADDED(listener: () => void, context?: any): void {
    Facade.global.dispatcher.on(LayoutEvents.VIEW_ADDED, listener, context);
  }

  public static OFF_VIEW_ADDED(listener: () => void, context?: any): void {
    Facade.global.dispatcher.off(LayoutEvents.VIEW_ADDED, listener, context);
  }
}
