import { IAssets, LoaderEvents } from "../../..";
import { Facade } from "../../../mmvc/facade/Facade";

export class LoaderEventsApi {
  private static dispatcher = Facade.global.dispatcher;

  public static EMIT_GROUP_LOADING_COMPLETE(group: keyof IAssets): void {
    this.dispatcher.emit(LoaderEvents.GROUP_LOADING_COMPLETE, group);
  }

  public static ON_GROUP_LOADING_COMPLETE(
    listener: (group?: keyof IAssets) => void,
    context?: any
  ): void {
    this.dispatcher.on(LoaderEvents.GROUP_LOADING_COMPLETE, listener, context);
  }

  public static EMIT_LOADING_COMPLETE(): void {
    this.dispatcher.emit(LoaderEvents.LOADING_COMPLETE);
  }

  public static ON_LOADING_COMPLETE(listener: () => void, context?: any): void {
    this.dispatcher.on(LoaderEvents.LOADING_COMPLETE, listener, context);
  }

  public static OFF_LOADING_COMPLETE(
    listener: () => void,
    context?: any
  ): void {
    this.dispatcher.off(LoaderEvents.LOADING_COMPLETE, listener, context);
  }

  public static EMIT_LOADING_PROGRESS(progress: number): void {
    this.dispatcher.emit(LoaderEvents.LOADING_PROGRESS, progress);
  }

  public static ON_LOADING_PROGRESS(
    listener: (progress: number) => void,
    context?: any
  ): void {
    this.dispatcher.on(LoaderEvents.LOADING_PROGRESS, listener, context);
  }

  public static OFF_LOADING_PROGRESS(
    listener: (progress: number) => void,
    context?: any
  ): void {
    this.dispatcher.off(LoaderEvents.LOADING_PROGRESS, listener, context);
  }
}
