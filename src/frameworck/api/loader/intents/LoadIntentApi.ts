import { Dictionary, Facade, IAssets, LoaderIntents } from "../../..";

export class LoadIntentApi {
  public static EMIT_LOAD(assets: IAssets): void {
    Facade.global.dispatcher.emit(LoaderIntents.LOAD, assets);
  }

  public static ON_LOAD(
    listener: (assets: IAssets) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.on(LoaderIntents.LOAD, listener, context);
  }

  public static OFF_LOAD(
    listener: (assets: IAssets) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.off(LoaderIntents.LOAD, listener, context);
  }
}
