import { Dictionary } from "../../../mmvc/interfaces";

export interface IAssets {
  preload?: Dictionary<string>;
  load?: Dictionary<string>;
  lazy?: Dictionary<string>; // not implemented yet
}

export const LoadingGroup: Record<keyof IAssets, keyof IAssets> = {
  preload: "preload",
  load: "load",
  lazy: "lazy",
};
