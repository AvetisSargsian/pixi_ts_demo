import { Facade } from "../../..";
import { UIElementsList } from "../UIElementsList";
import { UIEvents } from "./UIEvents";

export class UIEventsApi {
  public static EMIT_BUTTON_PRESSED(btnId: UIElementsList | number): void {
    Facade.global.dispatcher.emit(UIEvents.BUTTON_PRESSED, btnId);
  }

  public static ON_BUTTON_PRESSED(
    listener: (btnId: UIElementsList | number) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.on(UIEvents.BUTTON_PRESSED, listener, context);
  }

  public static OFF_BUTTON_PRESSED(
    listener: (btnId: UIElementsList | number) => void,
    context?: any
  ): void {
    Facade.global.dispatcher.off(UIEvents.BUTTON_PRESSED, listener, context);
  }
}
