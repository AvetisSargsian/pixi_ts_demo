import * as PIXI from "pixi.js";
import EventEmitter = PIXI.utils.EventEmitter;

export class Model extends EventEmitter {
  public dispose(): void {
    this.removeAllListeners();
  }
}
