import { Application } from "../../Application";
import { Container } from "pixi.js";

export class BaseView extends Container {
  public static readonly ADDED: string = "added";
  public static readonly REMOVED: string = "removed";

  protected resources = Application.RESOURCES();

  constructor() {
    super();
    this.resources;
    this.on(BaseView.ADDED, this.onAdded, this);
    this.on(BaseView.REMOVED, this.onRemoved, this);
  }

  public removeFromParent() {
    if (this.parent) {
      this.parent.removeChild(this);
    }
  }

  protected onAdded() {
    this.off(BaseView.ADDED, this.onAdded);
  }

  protected onRemoved() {
    this.off(BaseView.REMOVED, this.onRemoved);
  }
}
