// import mvc.view.BaseView;
// import starling.events.Event;

import { BaseView } from "../views/BaseView";

export class AbstractViewAbstractPool {
  private poolVec: any[];

  constructor() {
    this.poolVec = [];
  }

  public fillUp(count: number): void {
    for (var i: number = 0; i < count; ++i) {
      this.poolVec.push(this.createView());
    }
  }

  public take(): BaseView {
    let view: BaseView;

    if (this.poolVec.length > 0) {
      view = this.poolVec.pop();
    } else {
      view = this.createView();
    }
    // view.addEventListener(Event.REMOVED_FROM_STAGE, this.returnToPool.bind(this));
    return view;
  }

  public put(view: BaseView): void {
    this.poolVec.push(view);
  }

  public flush(): void {
    for (var i: number = 0, len: number = this.poolVec.length; i < len; ++i) {
      this.poolVec[i].dispose();
      this.poolVec[i] = null;
    }
    this.poolVec.length = 0;
  }

  public dispose(): void {
    this.poolVec = null;
  }

  protected createView(): BaseView {
    return new BaseView();
  }

  private returnToPool(event: Event): void {
    // var veiw: BaseView = event.target as BaseView;
    // veiw.removeEventListener(Event.REMOVED_FROM_STAGE, this.returnToPool.bind(this));
    // this.put(veiw);
  }
}
