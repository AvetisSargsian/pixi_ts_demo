import { IDisposeble } from "../interfaces/IDisposeble";
import { IContentProduct } from "../interfaces/IContentProduct";

export class ContentWraper implements IContentProduct<IDisposeble> {
  private _content: IDisposeble;

  public constructor(content: any) {
    this._content = content;
  }

  public get content(): any {
    return this._content;
  }

  public dispose(): void {
    this._content.dispose();
    this._content = null;
  }
}
