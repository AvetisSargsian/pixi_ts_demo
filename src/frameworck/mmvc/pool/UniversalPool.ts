import { Constructor } from "../interfaces/Constructor";
import { IContentProduct } from "../interfaces/IContentProduct";
import { IDisposeble } from "../interfaces/IDisposeble";
import { ContentWraper } from "./ContentWraper";

export class UniversalPool {
  private _poolVec: IContentProduct<IDisposeble>[] = [];
  private _classOfProduct: Constructor<any>;

  public fillUp(classOfProduct: any, count: number): void {
    this._classOfProduct = classOfProduct;
    for (var i: number = 0; i < count; ++i) {
      this._poolVec[this._poolVec.length] = this.createProduct(classOfProduct);
    }
  }

  public pull(): IContentProduct<IDisposeble> {
    let product: IContentProduct<IDisposeble>;
    if (this._poolVec.length > 0) {
      product = this._poolVec.pop();
    } else {
      product = this.createProduct(this._classOfProduct);
    }
    return product;
  }

  public push(product: IContentProduct<IDisposeble>): void {
    this._poolVec[this._poolVec.length] = product;
  }

  public flush(): void {
    for (var i = 0, len = this._poolVec.length; i < len; ++i) {
      this._poolVec[i].dispose();
      this._poolVec[i] = null;
    }
    this._poolVec.length = 0;
  }

  public dispose(): void {
    this._classOfProduct = null;
    this._poolVec = null;
  }

  protected createProduct(classOfProduct: any): IContentProduct<IDisposeble> {
    return new ContentWraper(new classOfProduct());
  }
}
