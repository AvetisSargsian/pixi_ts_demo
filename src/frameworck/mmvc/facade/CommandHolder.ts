import { BaseCommand, ClassConstructor, Facade } from "../..";

export class CommandHolder<T extends BaseCommand> {
  protected commands: Array<ClassConstructor<T>> = [];

  constructor(private facade: Facade) {}

  public addCommand(command: ClassConstructor<T>): boolean {
    if (this.commands.indexOf(command) < 0) {
      this.commands.push(command);
      return true;
    }
    return false;
  }

  public removeCommand(command: ClassConstructor<T>): boolean {
    const index: number = this.commands.indexOf(command);
    if (index > -1) {
      this.commands.splice(index, 1);
      return true;
    }
    return false;
  }

  public isEmpty(): boolean {
    return this.commands.length === 0;
  }

  public execute(data?: any): void {
    this.commands.forEach((command: ClassConstructor<T>) => {
      this.facade.getClassInstance<BaseCommand>(command).execute(data);
    });
  }
}
