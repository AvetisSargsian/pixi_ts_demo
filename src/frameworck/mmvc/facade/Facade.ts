import * as PIXI from "pixi.js";
import { BaseCommand } from "../..";
import { ClassConstructor } from "../interfaces";
import { IEvent } from "../interfaces";
import { ClassCreator } from "./ClassCreator";
import { CommandHolder } from "./CommandHolder";
import EventEmitter = PIXI.utils.EventEmitter;

export class Facade {
  private static readonly nameSpaceMap: Map<string, Facade> = new Map();
  private static _global: Facade;

  private readonly classesHolder: Map<any, ClassConstructor<any>> = new Map();
  private readonly eventsMap: Map<string, CommandHolder<BaseCommand>> =
    new Map();
  private readonly notificator: EventEmitter = new EventEmitter();
  private readonly creator: ClassCreator = new ClassCreator();
  private readonly singletons: Map<ClassConstructor<any>, any> = new Map();

  public static get global(): Facade {
    if (!this._global) {
      this._global = this.createNameSpace();
    }
    return this._global;
  }

  public static getSpace(key: string): Facade {
    return this.nameSpaceMap.get(key);
  }

  public static registerNameSpace(name: string): Facade {
    if (!this.nameSpaceMap.has(name)) {
      this.nameSpaceMap.set(name, this.createNameSpace());
    }
    return this.nameSpaceMap.get(name);
  }

  public static removeNameSpace(name): boolean {
    if (this.nameSpaceMap.has(name)) {
      this.nameSpaceMap.delete(name);
      return true;
    }
    return false;
  }

  public dispatch<T>(event: IEvent<T>): void {
    this.notificator.emit(event.name, event.data);
  }

  public get dispatcher(): EventEmitter {
    return this.notificator;
  }

  public registerSingleton<T>(key: ClassConstructor<T>, ...args: any[]): T {
    if (!this.singletons.has(key)) {
      const newInstance = this.creator.getNew(key, args);
      this.singletons.set(key, newInstance);
      return newInstance;
    } else {
      return this.singletons.get(key);
    }
  }

  public registerSingletonAs<T, K>(
    key: ClassConstructor<T>,
    constructor: ClassConstructor<K>,
    ...args: any[]
  ): K {
    if (this.singletons.has(key)) {
      this.singletons.get(key).dispose();
    }
    const newInstance = this.creator.getNew(constructor, args);

    this.singletons.set(key, newInstance);
    return newInstance;
  }

  public getSingleton<T>(key: ClassConstructor<T>): T {
    return this.singletons.get(key);
  }

  public deleteSingleton<T>(key: ClassConstructor<T>): boolean {
    if (this.singletons.has(key)) {
      this.singletons.get(key).dispose();
      this.singletons.delete(key);
      return true;
    }
    return false;
  }

  public registerClassAlias<I>(
    alias: any,
    classConstructor?: ClassConstructor<I>
  ): void {
    if (!classConstructor) {
      classConstructor = alias;
    }
    this.classesHolder.set(alias, classConstructor);
  }

  public getClassInstance<T>(alias: any, ...args: any[]): T | null {
    if (this.classesHolder.has(alias)) {
      return this.creator.getNew(this.classesHolder.get(alias), ...args);
    }
    console.warn("NO SUCH CLASS REGISTERED - ", alias);
    return null;
  }

  public deleteClass(alias: any): boolean {
    if (this.classesHolder.has(alias)) {
      this.classesHolder.delete(alias);
      return true;
    }
    return false;
  }

  public bindCommandToEvent<T extends BaseCommand>(
    eventName: string,
    constructors: ClassConstructor<T> | ClassConstructor<T>[]
  ): void {
    if (Array.isArray(constructors)) {
      constructors.forEach((constructor) =>
        this.bindOne(eventName, constructor)
      );
    } else {
      this.bindOne(eventName, constructors);
    }
  }

  protected bindOne<T extends BaseCommand>(
    eventName: string,
    constructor: ClassConstructor<T>
  ) {
    if (!this.classesHolder.has(constructor)) {
      this.registerClassAlias(constructor);
    }
    let holder: CommandHolder<BaseCommand>;
    if (!this.eventsMap.has(eventName)) {
      holder = new CommandHolder(this);
      this.eventsMap.set(eventName, holder);
      this.notificator.on(eventName, holder.execute, holder);
    } else {
      holder = this.eventsMap.get(eventName);
    }
    holder.addCommand(constructor);
  }

  public unbindCommandFromEvent<T extends BaseCommand>(
    eventName: string,
    constructor: ClassConstructor<T>
  ): void {
    const holder: CommandHolder<BaseCommand> = this.eventsMap.get(eventName);
    if (holder) {
      holder.removeCommand(constructor);
      if (holder.isEmpty()) {
        this.notificator.off(eventName, holder.execute, holder);
        this.eventsMap.delete(eventName);
      }
    }
  }

  protected static createNameSpace(): Facade {
    return new Facade();
  }
}
