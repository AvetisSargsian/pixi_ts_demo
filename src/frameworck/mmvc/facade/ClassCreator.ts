import { ClassConstructor } from "../interfaces";

export class ClassCreator {
  public getNew<T>(ctor: ClassConstructor<T>, ...args: any[]): T {
    return new ctor(...args);
  }
}
