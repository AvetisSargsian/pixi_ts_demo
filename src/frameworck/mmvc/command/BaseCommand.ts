import { ICommand } from "./interfaces/ICommand";

export class BaseCommand implements ICommand {
  public execute(data?: any): void {}
}
