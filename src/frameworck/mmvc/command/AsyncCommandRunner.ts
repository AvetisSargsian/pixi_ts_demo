import { AsyncCommand } from "./AsyncCommand";
import { IAsyncCommand } from "./interfaces/IAsyncCommand";
import { IAsyncCommandRunner } from "./interfaces/IAsyncCommandRunner";

export class AsyncCommandRunner
  extends AsyncCommand
  implements IAsyncCommandRunner
{
  protected _commandList: IAsyncCommand[];
  protected _currentCommand: IAsyncCommand;
  protected _garbageCollector: (command: IAsyncCommand) => any;
  protected _isPoused: boolean;
  protected _isBusy: boolean;

  public constructor(commandList: IAsyncCommand[] = null) {
    super();
    this._commandList = [];
    this.addList(commandList);
  }

  public clearList(): void {
    this._commandList.length = 0;
  }

  public completeCurent(): void {
    if (this._currentCommand) {
      this._currentCommand.complete();
    }
  }

  public completeCurrentAndPose(): void {
    this._isPoused = true;
    this.completeCurent();
  }

  public poseExecuting(): void {
    this._isPoused = true;
  }

  public addFirst(command: IAsyncCommand): void {
    this._commandList.unshift(command);
  }

  public addLast(command: IAsyncCommand): void {
    this._commandList[this._commandList.length] = command;
  }

  public addList(list: IAsyncCommand[]): void {
    if (!list || list.length == 0) {
      return;
    }
    this._commandList.concat(list);
  }

  public execute(data: Object = null): Promise<any> {
    this._isPoused = false;
    this.attemptToExecute();
    return Promise.resolve();
  }

  public dispose(): void {
    this._commandList = null;
    this._currentCommand = null;
    super.dispose();
  }

  public set garbageCollector(value: (command: IAsyncCommand) => any) {
    this._garbageCollector = value;
  }

  protected attemptToExecute(): void {
    if (!this._isBusy) {
      this.executeNext();
    }
  }

  protected throwInGarbage(command: IAsyncCommand): void {
    if (command) {
      command.dispose();
      if (this._garbageCollector) {
        this._garbageCollector(command);
      }
    }
  }

  protected executeNext(data: Object = null): void {
    this._isBusy = false;
    this.throwInGarbage(this._currentCommand);
    this._currentCommand = null;

    if (!this._isPoused && this._commandList && this._commandList.length > 0) {
      this._isBusy = true;
      this._currentCommand = this._commandList.shift();
      this._currentCommand.execute().then(this.executeNext);
    }
  }
}
