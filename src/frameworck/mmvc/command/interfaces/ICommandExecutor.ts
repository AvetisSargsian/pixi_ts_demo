import { ICommand } from "./ICommand";

export interface ICommandExecutor extends ICommand {
  loadCommands(vec: ICommand[]): ICommandExecutor;
  stop(): void;
  reset(): void;
}
