export interface ICommand {
  execute(data?: any): void;
}
