import { IDisposeble } from "../../interfaces";

export interface IAsyncCommand extends IDisposeble {
  execute(data?: any): Promise<any>;
  init(initParams?: object): IAsyncCommand;
  complete(): boolean;
}
