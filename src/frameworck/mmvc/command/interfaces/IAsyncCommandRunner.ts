import { IAsyncCommand } from "./IAsyncCommand";

export interface IAsyncCommandRunner extends IAsyncCommand {
  addFirst(command: IAsyncCommand): void;
  addLast(command: IAsyncCommand): void;
  addList(list: IAsyncCommand[]): void;
  clearList(): void;
  poseExecuting(): void;
  completeCurent(): void;
}
