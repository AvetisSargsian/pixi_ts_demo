import { IAsyncCommand } from "./interfaces/IAsyncCommand";

export class AsyncCommand implements IAsyncCommand {
  protected _initParams: any[];

  public init(initParams: object): IAsyncCommand {
    this._initParams = initParams as any[];
    return this;
  }

  public execute(data?: any): Promise<any> {
    return Promise.resolve(data);
  }

  public dispose(): void {
    this._initParams = null;
  }

  public complete(): boolean {
    return false;
  }
}
