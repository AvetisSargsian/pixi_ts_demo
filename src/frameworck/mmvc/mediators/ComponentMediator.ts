import { BaseMediator } from "./BaseMediator";
import { CompositeMediator } from "./CompositeMediator";

export class ComponentMediator extends BaseMediator {
  protected parent: ComponentMediator | null = null;

  public get parentMediator(): ComponentMediator | null {
    return this.parent;
  }

  public set parentMediator(value: ComponentMediator | null) {
    this.parent = value;
  }

  public removeFromParentMediator(): void {
    if (this.parentMediator) {
      (this.parentMediator as CompositeMediator).remove(this);
      this.parent = null;
    }
  }

  public update(data?: any): void {}

  public dispose(): void {
    this.removeFromParentMediator();
    super.dispose();
  }
}
