import { IDisposeble } from "../interfaces/IDisposeble";
import { BaseView } from "../views/BaseView";

export interface IMediator extends IDisposeble {
  getNativeVIew(): BaseView;
  getContextView(): BaseView | undefined;
  addToParent(parent: BaseView): void;
}
