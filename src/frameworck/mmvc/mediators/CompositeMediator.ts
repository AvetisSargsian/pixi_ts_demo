import { BaseView } from "../views/BaseView";
import { ComponentMediator } from "./ComponentMediator";
import { IMediator } from "./IMediator";

export class CompositeMediator extends ComponentMediator {
  private childrenStore: IMediator[] = [];

  public get children(): IMediator[] {
    return this.childrenStore;
  }

  public add(c: ComponentMediator): void {
    c.parentMediator = this;
    this.childrenStore[this.childrenStore.length] = c;
  }

  public remove(c: ComponentMediator): void {
    for (let i = 0, l = this.childrenStore.length; i < l; ++i) {
      if (c === this.childrenStore[i]) {
        c.parentMediator = null;
        this.childrenStore.splice(i, 1);
        break;
      }
    }
  }

  public getChild(n: number): IMediator | null {
    if (n >= 0 && n < this.childrenStore.length) {
      return this.childrenStore[n];
    } else {
      return null;
    }
  }

  public getChildrenCount(): number {
    return this.childrenStore.length;
  }

  public getMediatorByView(view: BaseView): IMediator | null {
    for (let i = 0, l = this.childrenStore.length; i < l; ++i) {
      const component: IMediator = this.childrenStore[i];
      if (component && component.getNativeVIew() === view) {
        return component;
      }
    }
    return null;
  }

  public update(data?: any) {
    for (let i = this.childrenStore.length - 1; i >= 0; --i) {
      const component: ComponentMediator = this.childrenStore[
        i
      ] as ComponentMediator;
      if (component) {
        component.update(data);
      }
    }
  }

  public dispose(): void {
    for (let i = this.childrenStore.length - 1; i >= 0; --i) {
      const component: IMediator = this.childrenStore[i];
      if (component) {
        component.dispose();
      }
    }
    super.dispose();
  }
}
