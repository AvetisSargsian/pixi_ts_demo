import { BaseView } from "../views/BaseView";
import { IMediator } from "./IMediator";

export class BaseMediator implements IMediator {
  protected contextView: BaseView;
  protected nativeVIew: BaseView;

  public constructor(view?: BaseView) {
    if (view instanceof BaseView) {
      this.nativeVIew = view;
      if (this.nativeVIew.parent) {
        this.contextView = this.nativeVIew.parent as BaseView;
      } else {
        this.addListeners();
      }
    } else {
      this.nativeVIew = this.setNativeVIew();
      this.addListeners();
    }
  }

  public dispose(): void {
    this.nativeVIew.removeAllListeners();
    this.nativeVIew.removeFromParent();
  }

  public getNativeVIew(): BaseView {
    return this.nativeVIew;
  }

  public getContextView(): BaseView {
    if (!this.contextView && this.nativeVIew.parent) {
      this.contextView = this.nativeVIew.parent as BaseView;
    }
    return this.contextView;
  }

  public addToParent(parent: BaseView): void {
    if (this.nativeVIew && parent) {
      this.contextView = parent;
      this.contextView.addChild(this.nativeVIew);
    }
  }

  protected addListeners(): void {
    this.nativeVIew.on(BaseView.ADDED, this.onAddedToStage.bind(this));
    this.nativeVIew.on(BaseView.REMOVED, this.onRemovedFromStage.bind(this));
  }

  protected setNativeVIew(): BaseView {
    return new BaseView();
  }

  protected onRemovedFromStage(): void {
    this.nativeVIew.off(BaseView.REMOVED, this.onRemovedFromStage);
  }

  protected onAddedToStage(): void {
    this.nativeVIew.off(BaseView.ADDED, this.onAddedToStage);
  }
}
