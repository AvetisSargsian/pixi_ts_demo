import { BaseView } from "../views/BaseView";
import { SceneView } from "../views/SceneView";
import { CompositeMediator } from "./CompositeMediator";

export class SceneMediator extends CompositeMediator {
  public showOnStage(canvas: BaseView): void {
    this.addToParent(canvas);
  }

  public removeFromStage(): Promise<void> {
    this.dispose();
    return Promise.resolve();
  }

  public dispose(): void {
    // _nativeVIew.stage.removeEventListener(KeyboardEvent.KEY_DOWN,onKeyDown);
    super.dispose();
  }

  protected onAddedToStage(): void {
    super.onAddedToStage();
    // this.nativeVIew.stage.addEventListener(KeyboardEvent.KEY_DOWN,onKeyDown);
  }

  protected setNativeVIew(): BaseView {
    return new SceneView();
  }

  protected onKeyboardBACK(): void {}

  protected onKeyDown(): void {}
}
