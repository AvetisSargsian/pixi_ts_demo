export interface IModuleActivator {
  activate(data?: any): void;
}
