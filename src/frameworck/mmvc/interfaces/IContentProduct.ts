import { IDisposeble } from "./IDisposeble";

export interface IContentProduct<T extends IDisposeble> {
  content: T;
  dispose(): void;
}
