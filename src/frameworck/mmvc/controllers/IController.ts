export interface IController {
  startTick(): void;
  stopTick(): void;
}
