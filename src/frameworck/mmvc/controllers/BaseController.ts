import { Ticker } from "pixi.js";
import { Application } from "../../Application";
import { AbstractController } from "./AbstractController";

export class BaseController extends AbstractController {
  protected ticker: Ticker;

  constructor() {
    super();
    this.ticker = Application.TICKER();
  }

  public dispose(): void {
    this.stopTick();
  }

  public startTick(): void {
    this.ticker.add(this.tick, this);
  }

  public stopTick(): void {
    this.ticker.remove(this.tick, this);
  }

  protected tick(dt: number): void {}
}
