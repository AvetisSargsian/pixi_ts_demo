import { IController, IDisposeble } from "../..";

export abstract class AbstractController implements IController, IDisposeble {
  public abstract dispose(): void;

  public abstract startTick(): void;

  public abstract stopTick(): void;

  protected abstract tick(dt: number): void;
}
