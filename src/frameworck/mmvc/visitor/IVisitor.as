package mvc.visitor {
	public interface IVisitor {
		function name():String;
		function visit(accepter:Object):void;
	}
}