package mvc.visitor {
	public class Accepter implements IAccepter{
		public function accept(visiter:IVisitor):void {
			
//			когда обьект принимает "visitor"-а, то передаёт в метод интерфейса
//			себя для сбора или изменения данных в самом обьекте.
//			таким образом обьект может принимаить любого "visitor"-а, а те в свою 
//			очередь могут совершать разные операции над обьектом
//			так же в "visitor"-а могут быть вынесены обшие для разных обьектов методы...
//			video tutorials java-  https://www.youtube.com/watch?v=pL4mOUDi54o
//							as3-   https://www.youtube.com/watch?v=KSEyIXnknoY&index=4&list=PL8B19C3040F6381A2 
			visiter.visit(this);
		}
	}
}