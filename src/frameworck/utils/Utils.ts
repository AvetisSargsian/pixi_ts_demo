import { TweenLite } from "gsap";
import { Dictionary } from "..";

export class Utils {
  public static setTimeout(
    handler: (...args: any[]) => void,
    timeout: number,
    scope?: any,
    ...args: any[]
  ): number {
    const id = this.getId();
    this.timers[id] = TweenLite.delayedCall(
      timeout / 1000,
      handler,
      args,
      scope
    );
    return id;
  }

  public static clearTimeout(id: number): void {
    this.clearById(id);
  }

  public static setInterval(
    handler: () => void,
    timeout: number,
    scope?: any,
    ...args: []
  ): number {
    const id = this.getId();
    const tl = TweenLite.delayedCall(timeout / 1000, () => {
      tl.restart(true, true);
      handler.apply(scope, args);
    });
    this.timers[id] = tl;
    return id;
  }

  public static clearInterval(id: number): void {
    this.clearById(id);
  }

  public static tweenTo(target: any, duration: number, vars: any): TweenLite {
    return TweenLite.to(target, duration, vars);
  }

  protected static lastId: number = 0;
  protected static maxInteger: number;

  protected static clearById(id: number) {
    const tl = this.timers[id];
    if (tl) {
      tl.kill();
      delete this.timers[id];
    }
  }

  protected static getId() {
    // (Number as any) is used to prevent failing of compiler,
    // take into account, that is Number.MAX_SAFE_INTEGER returns undefined,
    // and the next Math.pow result will be taken
    this.maxInteger =
      this.maxInteger ||
      (Number as any).MAX_SAFE_INTEGER ||
      Math.pow(2, 53) - 1;
    if (this.lastId >= this.maxInteger) {
      this.lastId = 0;
    }
    return this.lastId++;
  }
  private static readonly timers: Dictionary<TweenLite> = {};
}
