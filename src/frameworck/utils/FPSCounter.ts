import * as PIXI from "pixi.js";
import { LayoutIntentsApi } from "..";
import { LayoutList } from "../api/layout/LayoutList";

export class FPSCounter {
  public readonly FPS: string = "fps:";
  public readonly textStyle = new PIXI.TextStyle({
    fontFamily: "Ubuntu",
    fontSize: 50,
    fill: "#ff0000",
    align: "center",
  });

  private fps = 0;
  private prevTime: number = 0;
  private currTime: number = 0;
  private readonly tf: PIXI.Text;

  constructor() {
    this.tf = new PIXI.Text("", this.textStyle);
    LayoutIntentsApi.EMIT_ADD_VIEW(LayoutList.TOP, this.tf);
  }

  public update() {
    const time = performance.now();
    this.currTime += time - this.prevTime;
    this.fps++;
    if (this.currTime >= 1000) {
      this.tf.text = this.FPS + this.fps;
      this.fps = 0;
      this.currTime = 0;
    }
    this.prevTime = time;
  }
}
