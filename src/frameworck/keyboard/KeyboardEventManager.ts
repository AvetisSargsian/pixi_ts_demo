export class KeyboardEventManager {
  private isDown: boolean = false;
  private isUp: boolean = true;
  private press: (event: KeyboardEvent) => void = undefined;
  private release: (event: KeyboardEvent) => void = undefined;

  constructor(private keyValue: string, private keystrokes: boolean = true) {
    window.addEventListener("keydown", this.downHandler.bind(this), false);
    window.addEventListener("keyup", this.upHandler.bind(this), false);
  }

  public dispose(): void {
    window.removeEventListener("keydown", this.downHandler.bind(this));
    window.removeEventListener("keyup", this.upHandler.bind(this));
  }

  public set onKeyDown(handler: (event: KeyboardEvent) => void) {
    this.press = handler;
  }

  public set onKeyUp(handler: (event: KeyboardEvent) => void) {
    this.release = handler;
  }

  private downHandler(event: KeyboardEvent): void {
    if (event.key === this.keyValue) {
      if (this.isUp && this.press) {
        this.press(event);
      }
      this.isDown = true;
      if (this.keystrokes) {
        this.isUp = false;
      }

      event.preventDefault();
    }
  }

  private upHandler(event: KeyboardEvent): void {
    if (event.key === this.keyValue) {
      if (this.isDown && this.release) {
        this.release(event);
      }
      this.isDown = false;
      this.isUp = true;
      event.preventDefault();
    }
  }
}
