import { Application } from "./frameworck";
import { MainModuleActivator as MainModulesActivator } from "./game/MainModulesActivator";

Application.GAME_HEIGHT = 768 * 1;
Application.GAME_WIDTH = 1366 * 1;

const gameDiv = document.createElement("div");
gameDiv.innerHTML = "";
gameDiv.id = "frame";
document.body.appendChild(gameDiv);

const app: Application = new Application(
  gameDiv,
  Application.GAME_WIDTH,
  Application.GAME_HEIGHT
);
app.start(new MainModulesActivator());
