const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const webpack = require("webpack");

const ROOT_DIR = process.cwd();

module.exports = (env) => {
  console.log("env", env);
  console.log("os ->", process.platform);

  const nodeEnv = env.mode || "development";
  const platform = env.platform || "web";
  const isMobile = platform === "mobile";
  const isProduction = env.mode === "production";

  const patterns = [];
  if (platform === "web") {
    patterns.push({ from: "./assets", to: "./assets" });
  }

  let OS;
  if (/^win/.test(process.platform)) {
    OS = "Windows";
  } else if (process.platform === "darwin") {
    OS = "MAC";
  } else if (process.platform === "linux") {
    OS = "Linux";
  }

  return {
    mode: nodeEnv,
    devtool: isProduction ? false : "source-map",
    optimization: {
      emitOnErrors: false,
      minimize: isProduction,
      moduleIds: "named",
    },
    devServer: {
      open: true,
      port: 3003,
    },
    entry: "./src/main.ts",
    output: {
      filename: "main.js",
      path: path.resolve(__dirname, "dist"),
    },
    plugins: [
      new CleanWebpackPlugin(),
      new webpack.ProvidePlugin({
        PIXI: "pixi.js",
        "pixi-spine": "pixi-spine",
      }),
      new CopyWebpackPlugin({ patterns }),
      new webpack.DefinePlugin({
        __DEV__: !isProduction,
        __PROD__: isProduction,
        __PLATFORM__: JSON.stringify(platform),
        __DESKTOP__: platform === "desktop",
        __OS__: JSON.stringify(OS),
      }),
      new HtmlWebpackPlugin({
        template: "./src/index.html",
        title: "TestTask",
      }),
    ],
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ["style-loader", "css-loader"],
        },
        {
          test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
          type: "asset",
        },
        {
          test: /\.tsx?$/,
          use: "ts-loader",
          include: path.resolve(__dirname, "src"),
        },
      ],
    },
    resolve: {
      alias: {
        "pixi.js": path.resolve(__dirname, "./node_modules/pixi.js"),
        PIXI: path.resolve(__dirname, "./node_modules/pixi.js"),
        "pixi-spine": path.resolve(__dirname, "./node_modules/pixi-spine"),
      },

      fallback: {
        fs: false,
      },

      extensions: [".tsx", ".ts", ".js"],
    },
  };
};
