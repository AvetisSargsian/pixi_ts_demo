# AdVenture Capitalist clone

This project was created with pixi.js and one carrot image from www.freepick.com

# How to run

You can check out the online version of the project at:

https://graphium-studio.itch.io/testtaskdemo?secret=elXqGOoaLUoP4R15aWbm8AxXCuA

or you can run project on you local machine:

`npm run w-start-dev` - Runs the app in the development mode.
Open http://localhost:3003/ to view it in the browser.
The page will reload if you make edits.

`npm run w-build-prod` - Builds the app for production
`npm run server` - to run production ready application

# Project overview:

    The is a collection of different test tasks gathered in one application.
    The purpose of this app is to demonstrate architectural solutions for building flexible and scaleble interactive games.
    UI and graphical parts of application is left on elementary level as they play secondary role.

# Code structure

    The basis of the application is a framework that allows individual functional modules to interact with each other and remain independent of each other at the same time.
    This approach allows to have "loose coupling" between components, making it easy to scale and modify the application.
    A game is built on top of the framework and divided into scenes in which game events take place.
    Each functional module of the application uses the MMVС architecture.
    The transfer of requests and data between modules is carried out by events transmitted through the common eventbus of the framework.

# Known issues

    Because application is in development stage, it is possible to observe some unexpected behaviour while playing game.

# Improvements

- texture atlasses
- sounds
- effect
- additional features
- particles
- add server and server checking
- etc.
